<%@page import="java.util.*"%>
<%@page import="Entities.*"%>
<%@page import="DAO.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Giỏ Hàng</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <!-- bootstrap -->
        <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
   <link href="style/style.css" rel="stylesheet" />
   <link href="style/menu_style.css" rel="stylesheet" />
   <link href="style/stylesheet1.css" rel="stylesheet" />
   <script src="script/jquery.js" type="text/javascript"></script>
   <script src="script/script.js" type="text/javascript"></script>
   
    </head>
    <body>
        <div class="container">
            <div class="header">
                
                 <%@include file="header.jsp" %>
                </div>
            
            <!--header-->
            <br>
            <div class="menu">
              <%@include file="menungang.jsp" %>
            </div>
                
            <!--menu-->
            <br>
            <div id="content"> 
  <h1>Giỏ hàng        &nbsp;
      </h1>
 
    <div class="cart-info">
      <table>
        <thead>
          <tr>
            <td class="image">Hình ảnh</td>
            <td class="name">Sản phẩm</td>
            <td class="model">Mã</td>
            <td class="quantity">Số lượng</td>
            <td class="price">Giá</td>
            <td class="total">Tổng</td>
          </tr>
        </thead>
        <tbody>
           <% 
           Object obj= session.getAttribute("giohang");
           //kiem tra obj null ko
           ArrayList<ChiTietGioHang> _danhsachgiohang = new ArrayList<ChiTietGioHang>();
           float tongtien=0;
           if(obj!= null)
           {
                    //Ep kieu obj thanh ArrayList<Chitietgiohang>
                    _danhsachgiohang = (ArrayList<ChiTietGioHang>) obj;
                    //dung for chay
                    for(int i=0; i<_danhsachgiohang.size();i++)
                     {
                         // Toi mỗi chi tiết lấy sản phẩm theo masp co dc
                         // in cac thông tin sản phâm ra
                         DAO.SanphamDAO _sanpham = new SanphamDAO();
                         Sanpham c = _sanpham.getSPByid(_danhsachgiohang.get(i).masanpham);
                         float tong = c.getGia()*_danhsachgiohang.get(i).soluong;
                         tongtien=tongtien+tong;
                         %>
                                <tr>
                                 <td class="image">
                                     <a href="#"><img src="<%=c.getHinhAnh()%>" width="50px" height="50px"></a>
                                 </td>
                                 <td class="name">
                                     <a href="#"><%=c.getTenSp()%></a>
                                                 <div>
                                                   </div>
                                   </td>
                                 <td class="model"><%=c.getMaSp()%></td>
                                 <td class="quantity"><input type="text" name="quantity[79]" id="soluong<%=c.getMaSp()%>" value="<%=_danhsachgiohang.get(i).soluong%>" size="1">
                                   &nbsp;
                                   <button id="suagiohang<%=c.getMaSp()%>"> Sua </button>
                                     <script>
                                     $( "#suagiohang<%=c.getMaSp()%>" ).click(function() {

                                         var sl =  $( "#soluong<%=c.getMaSp()%>" ).val();
                                         
                                                          $.ajax({
                                         type: "POST",
                                         url: "ThemvaogiohangController",
                                         data: "massp=<%=c.getMaSp()%>&sl="+sl,
                                         dataType: "text",
                                         cache: false,
                                         timeout: 60,
                                         success: function(data) {
                                             alert("Sửa thành công");
                                             location.reload();
                                         }});
                                       });

                                     </script>
                                   &nbsp;
                                   <button id="xoa<%=c.getMaSp()%>" >Xoa</button>
                                            <script>
                                              $( "#xoa<%=c.getMaSp()%>" ).click(function() {
                                                                   $.ajax({
                                                  type: "POST",
                                                  url: "XoakhoigiohangController",
                                                  data: "massp=<%=c.getMaSp()%>",
                                                  dataType: "text",
                                                  cache: false,
                                                  timeout: 60,
                                                  success: function(data) {
                                                      alert("Xóa thành công");
                                                      location.reload();
                                                  }});
                                                });

                                              </script>
                                 </td>
                                 <td class="price"><%=c.getGia()%> VND</td>
                                 <td class="total"><%=tong%> VND</td>
                                 
                               </tr>
            <%
                     }
           } 
           else 
               out.println("Không có sản phâm nào trong giỏ hàng");
           %>          
          
                    
                            </tbody>
      </table>
    </div>
  
    <div class="cart-total">
    <table id="total">
            <tbody>
            <tr>
        <td class="right"><b>Tổng cộng ::</b></td>
        <td class="right"><%=tongtien%></td>
      </tr>
          </tbody></table>
  </div>
  <div class="buttons">
    <div class="right"><a href="#" class="btn btn-danger">Thanh toán</a></div>
    <div class="center"><a href="menu.jsp" class="btn btn-primary">Tiếp tục mua hàng</a></div>
  </div>
  </div>     
            <!--content-->
            <br>
            <div class="footer">
                <div class="container">
                    <h3><label class="label label-success">SWEET COFFEE SHOP</label></h3>
                </div>
            </div>
            <!--footer-->
        </div>
        <script type="text/javascript" src="script/jquery-min.js"></script>
    </body>
</html>

