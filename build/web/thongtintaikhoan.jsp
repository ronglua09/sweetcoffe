<%@page import="DAO.KhachhangDAO"%>
<%@page import="Entities.Khachhang"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Entities.Nguoidung"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Menu chi tiet</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <!-- bootstrap -->
        <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
   <link href="style/style.css" rel="stylesheet" />
   <link href="style/menu_style.css" rel="stylesheet" />
   <script src="script/jquery.js" type="text/javascript"></script>
   <script src="script/script.js" type="text/javascript"></script>
   
    </head>
    <body>
        <div class="container">
            <div class="header">
                 
                <div class="row">
                    <div class="col-md-6">
                        <a href="index.jsp">
                            <img src="img/logotrong.png"></a>
                             <div id="title">
                                 Chút ngọt cho ly cà phê đắng!
                             </div>
                             
                    </div>
                    <div class="col-md-6">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">Hướng Dẫn Mua Hàng</a></li>
                            <li><a href="#">Thông Tin Vận Chuyển</a></li>
                             <%
       
       Nguoidung acc = null;
       try{
           
       if("logout".equals(request.getParameter("action")))
       {
           session.removeAttribute("users");
       }    
       
       acc = (Nguoidung) session.getAttribute("users");
       }catch(Exception e){}
       
       if(acc == null)
       {
           %>
                            <li><a href="Dangnhap.jsp">Đăng Nhập</a></li>
                            <%}else{%>  
                            <li><a href="thongtintaikhoan.jsp"><%=acc.getUsername()%></a></li>
                            <%}%>
                            <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Giỏ hàng</a></li>
                        </ul>
                        <form class="navbar-form navbar-left pull-right" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Nhập tên sản phẩm">
                            </div>
                            <button type="submit" class="btn btn-default">Tìm kiếm</button>
                        </form> 
                        
                    </div>
                </div>
           
            </div>
            <!--header-->
            <br>
            <div class="menu">
               <%@include file="menungang.jsp" %>
 
            </div>
                
            <!--menu-->
            <br>
            <div class="content">
                <div id="title">
                                 >> Thông tin tài khoản
                             </div>
                <br>
               <div class="contact">
                    <div id="nhapdulieu">
                        <div id="kh_left">
                            <br>
                            Họ Tên: 
                            <br><br><br>
                            Địa chỉ:
                            <br><br><br>
                             Giới tính:
                            <br><br><br>
                            Email :
                            <br><br>
                            Số Điên Thoại:
                            <br><br>
                           
                        </div>
                        <div id="kh_right">
                               
                             <%
       if(acc!=null){
       KhachhangDAO khdao=new KhachhangDAO();
       Khachhang kh=khdao.getNguoidungByuser(acc.getUsername());
       %>
                             <br>
                             <input type="text" class="form-control" id="hoten" name="hoten" value="<%=kh.getHoTen()%>" style="width: 300px;"> 
                            <br>
                            <select class="form-control" name="diachi" style="width: 300px;" >
                                <option><%=kh.getDiaChi()%></option>
                                
                            </select>  <br>
                              <select class="form-control" name="gioitinh"  style="width: 300px;" >
                                <option><%=kh.getGioiTinh()%></option>
                                
                            </select> <br>
                            <input type="email" class="form-control" id="email" name="email" value="<%=kh.getEmail()%>" style="width: 300px;">
                           <br>
                           <input type="text" class="form-control" id="sdt" name="sdt" value="<%=kh.getSdt()%>" style="width: 300px;">
                          
                        </div>
                           <%}else{%>
                           người dùng không tồn tại<%}%>
                    </div>
 
            </div>
                
            <!--content-->
            <br>
            <div class="footer">
                <div class="container">
                    <h3><label class="label label-success">SWEET COFFEE SHOP</label></h3>
                </div>
            </div>
            <!--footer-->
        </div>
        <script type="text/javascript" src="script/jquery-min.js"></script>
    </body>
</html>


