

<%@page import="java.util.*"%>
<%@page import="Entities.*"%>
<%@page import="DAO.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Menu chi tiet</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <!-- bootstrap -->
        <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
   <link href="style/style.css" rel="stylesheet" />
   <link href="style/menu_style.css" rel="stylesheet" />
   <script src="script/jquery.js" type="text/javascript"></script>
   <script src="script/script.js" type="text/javascript"></script>
   <script language="javascript">

</script>
    </head>
    <body>
        <div class="container">
            <div class="header">
                
                 <%@include file="header.jsp" %>
                </div>
            
            <!--header-->
            <br>
            <div class="menu">
               <ul class="nav nav-pills">
                   <li><a href="trangchu.jsp">Home</a></li>
                   <li><a href="menu.jsp">Menu</a></li>
                     <li><a href="#">Albums</a></li>
                     <li><a href="blogs.jsp">Blogs</a></li>
                     <li><a href="danhsachtintuc.jsp">Tin Tức</a></li>
                       <li><a href="#">Liên Hệ</a></li>
   
</ul>
 
            </div>
                
            <!--menu-->
            <br>
            <div class="content">
                <div id="title">
                                 >> Menu Sản Phẩm 
                             </div>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-8">
                         <jsp:useBean id="_loaisp" class="DAO.LoaispDAO" scope="page"/>          
                                                            <% 
                                                                SanphamDAO sp=new SanphamDAO();
                                                                 
                                                            Object obj=session.getAttribute("danhsachxem");
                                                                if(obj!=null)
                                                                 {
                                                                 ArrayList<Integer> ds = (ArrayList<Integer>) obj;
                                                                    Integer malsp = 1;
                                                                    malsp = ds.get(ds.size()-1);
                                                                        if(malsp != null)
                                                                         {
                                                                        List<Sanpham> _list=sp.getSPByMaloaiSP(malsp);
                                                                        for(Sanpham c:_list){
                                                                            
                                                                        %>
                        <div class="product">
                        <div class="grow pic">
                             <form action="LoaispController" method="POST">
                                      <input type="hidden" name="getMaSp" value="<%=c.getMaSp()%>"/>
                                      <a href="chitietsanpham.jsp?masp=<%=c.getMaSp()%>"><img src="<%=c.getHinhAnh()%>" alt="portrait"></a>
                             </form>
                        </div>
                            <div id="name"><%=c.getTenSp()%></div></div>
                             <%
                                                                  }
          }}
          %>
           </div>
                          
            
                    <!--product-->
                  <!--col-xs-12 col-sm-6 col-md-8-->
                  <div class="col-xs-6 col-md-4">
                       <div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title"><div id="title">Tin nổi bật</div></h3>
                        </div>
                        <div class="panel-body">
                            <h4 class="media-heading"><a href="">Cà phê và 11 điều có thể bạn chưa biết
                            <br/><small>26/05/2014 - Đặng Quốc Đạt</small></a></h4>
                            <p>Đã bao giờ bạn bơi trong một bồn cà phê chưa? Hay đã có lần nào bạn nghĩ tới chuyện uống cà phê bằng… đĩa? Nếu chưa thì hãy mở mang tầm nhìn với những điều mới mẻ về cà phê cùng</p>
                            <h4 class="media-heading"><a href="">Con đường cà phê - Cà phê cội
			    <br/><small>26/05/2014 - Đặng Quốc Đạt</small></a></h4>
			    <p>Nằm ở độ cao từ 400 tới 500m so với mực nước biển, với lợi thế là vùng đất đỏ bazan màu mở và vùng khí hậu thích hợp, Buôn Ma Thuộc – tỉnh Đắk Lắk từ lâu được biết đến như là thủ phủ cà phê Robusta của Việt Nam </p>
                        </div>
                       </div>
                       <div class="panel panel-default">
                         <div class="panel-heading"><div id="title">Liên Kiết</div></div>
                          <div class="panel-body">
                            Like Facebook
                          </div>
                          </div>
                        
                      </div>
                  </div>
</div><!--row-->
                
            <!--content-->
            <br>
            <div class="footer">
                <div class="container">
                    <h3><label class="label label-success">SWEET COFFEE SHOP</label></h3>
                </div>
            </div>
            <!--footer-->
        </div>
        <script type="text/javascript" src="script/jquery-min.js"></script>
    </body>
</html>


