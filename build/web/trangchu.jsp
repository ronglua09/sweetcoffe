<%-- 
    Document   : trangchu
    Created on : Jun 5, 2014, 2:29:55 AM
    Author     : QUOCDAT_HT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>SWEET COFFEE</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <!-- bootstrap -->
        <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
   <link href="style/style.css" rel="stylesheet" />
   <script src="script/jquery.js" type="text/javascript"></script>
   <script src="script/script.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="container">
            <div class="header">
                
                <%@include file="header.jsp" %>
                </div>
            
            <!--header-->
            <br>
            <div class="menu">
                <%@include file="menungang.jsp" %>
 
            </div>
                
            <!--menu-->
            <br>
            <div class="content">
               
                    <div class="head-content">
                        <div class="row" style="height: 200px;">
                            <div class="col-md-8">
                                <div class="slide">
                                    <img src="img/1348068761-ca-phe-cappuccino.jpg">
                                    <img src="img/600-400-sang-tao-voi-tao-pho-ha-noi-1b8a.jpg">
                                    <img src="img/5954c802c6f2ac7c3253132aaed1420e.jpg">
                                    <img src="img/giam-can-bang-ca-phe.jpg">
                                </div>
                                <!--slide-->
                            </div>
                            <div class="col-md-4">
                                <div class="sale">
                                    <br>
                                    <br>
                                     Rộn Ràng Siêu Khuyến Mãi Chào Mừng Ngày 8/3!
                                     Giảm 5% tất cả sản phẩm
                                     <br>
                                     <button type="button" class="btn btn-danger">Xem Ngay</button>
                                </div><!--sale-->
                            </div>
                        </div>
                    </div><!--head-content-->
                    <br>
                    <div class="center-conten">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="noibat">
                                    <div id="imges">
                                        <img src="img/chude/1.png">
                                    </div>
                                    <div id="chude">
                                        <div id="td">
                                            <a href="">Khám phá các loại cà phê trên thế giới</a></div>
                                        <br>
                                        <div id="dt">
                                            <a href="danhsachtintuc.jsp">>>Xem thêm tin cà phê<<</a>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                             <div class="col-md-4">
                                 <div class="noibat">
                                    <div id="imges">
                                        <img src="img/bg.jpg" style="height: 172px;width: 371px;">
                                    </div>
                                    <div id="chude">
                                        <div id="td">
                                            <a href="">Sweet Coffe - Sự đa dạng của cà phê</a></div>
                                        <br>
                                        <div id="dt">
                                             <a href="">>>Giới thiệu về quán<<</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-4">
                                  <div class="noibat">
                                    <div id="imges">
                                        <img src="img/chude/quan_ca_phe_thu_cung_doc_dao_du_gioi_tre_sai_thanh_7.jpg" style="height: 172px;width: 371px;">
                                    </div>
                                    <div id="chude">
                                        <div id="td">
                                            <a href="">Những khoảng khắc kỷ niệm..</a></div>
                                        <br>
                                        <div id="dt">
                                             <a href="">>>Xem thêm Album ảnh<<</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div><!--center-conten-->
                    <br>
                    <div class="product">
                   <div id="tit"><span class="glyphicon glyphicon-star-empty">
                            SẢN PHẨM NỔI BẬT
                       </span></div><br>
                       <div class="row" style="border-top: 3px solid #000;margin-left: 8px;margin-right: 8px;">
                           <br>
                           <div class="row">
			<div class="col-md-3">
				<div class="thumbnail">
                                    <img src="img/1348068761-ca-phe-cappuccino.jpg" alt="...">
					<div class="caption">
                                            <div id="tieude">
                                                <a href="#">Caffe Capuchino</a>
                                                
                                            </div>
                                             Cà phê pha chút ngọt ngào của vị CAPUCHINO
                                            <div id="Subdrop">
                                                Cà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn
                                            </div> 
                                            
                                        </div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="thumbnail">
                                    <img src="img/1348068761-ca-phe-cappuccino.jpg" alt="...">
					<div class="caption">
					 <div id="tieude">
                                                <a href="#">Caffe Capuchino</a>
                                            </div>
                                            Cà phê pha chút ngọt ngào của vị CAPUCHINO
                                            <div id="Subdrop">
                                                Cà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn
                                            </div> 	
                                        </div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="thumbnail">
                                    <img src="img/1348068761-ca-phe-cappuccino.jpg" alt="...">
					<div class="caption">
					 <div id="tieude">
                                                <a href="#">Caffe Capuchino</a>
                                            </div>
                                            Cà phê pha chút ngọt ngào của vị CAPUCHINO
                                            <div id="Subdrop">
                                                Cà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn
                                            </div> 
                                        </div>
				</div>
                        </div>
                               
                           
			<div class="col-md-3">
				<div class="thumbnail">
                                    <img src="img/1348068761-ca-phe-cappuccino.jpg" alt="...">
					<div class="caption">
					 <div id="tieude">
                                                <a href="#">Caffe Capuchino</a>
                                            </div>
                                            Cà phê pha chút ngọt ngào của vị CAPUCHINO
                                            <div id="Subdrop">
                                                Cà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn
                                            </div> 	
                                        </div>
				</div>
			</div>
		</div>
                           <br>
                                <div class="row">
			<div class="col-md-3">
				<div class="thumbnail">
                                    <img src="img/1348068761-ca-phe-cappuccino.jpg" alt="...">
					<div class="caption">
                                            <div id="tieude">
                                                <a href="#">Caffe Capuchino</a>
                                            </div>
                                            Cà phê pha chút ngọt ngào của vị CAPUCHINO
                                           <div id="Subdrop">
                                                Cà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn
                                            </div> 
                                            
                                        </div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="thumbnail">
                                    <img src="img/1348068761-ca-phe-cappuccino.jpg" alt="...">
					<div class="caption">
					 <div id="tieude">
                                                <a href="#">Caffe Capuchino</a>
                                            </div>
                                            Cà phê pha chút ngọt ngào của vị CAPUCHINO
                                            <div id="Subdrop">
                                                Cà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn
                                            </div> 	
                                        </div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="thumbnail">
                                    <img src="img/1348068761-ca-phe-cappuccino.jpg" alt="...">
					<div class="caption">
					 <div id="tieude">
                                                <a href="#">Caffe Capuchino</a>
                                            </div>
                                            Cà phê pha chút ngọt ngào của vị CAPUCHINO
                                           <div id="Subdrop">
                                                Cà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn
                                            </div> 
                                        </div>
				</div>
			</div>
                              
                               
			<div class="col-md-3">
				<div class="thumbnail">
                                    <img src="img/1348068761-ca-phe-cappuccino.jpg" alt="...">
					<div class="caption">
					 <div id="tieude">
                                                <a href="#">Caffe Capuchino</a>
                                            </div>
                                            Cà phê pha chút ngọt ngào của vị CAPUCHINO
                                            <div id="Subdrop">
                                                Cà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn
                                            </div> 	
                                        </div>
				</div>
			</div>
		</div>
                       </div>
                            </div><!--product-->
                        </div>
                       
                   
           
            <!--content-->
            <br>
            <div class="footer">
                <div class="container">
                    <h3><label class="label label-success">SWEET COFFEE SHOP</label></h3>
                </div>
            </div>
            <!--footer-->
        </div>
        <script type="text/javascript" src="script/jquery-min.js"></script>
    </body>
</html>

