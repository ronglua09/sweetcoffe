<%@page import="java.util.*"%>
<%@page import="Entities.*"%>
<%@page import="DAO.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sản Phẩm</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <!-- bootstrap -->
        <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
   <link href="style/style.css" rel="stylesheet" />
   <link href="style/menu_style.css" rel="stylesheet" />
   <script src="script/jquery.js" type="text/javascript"></script>
   <script src="script/script.js" type="text/javascript"></script>
   
    </head>
    <body>
        <div class="container">
            <div class="header">
                
                 <%@include file="header.jsp" %>
                </div>
            
            <!--header-->
            <br>
            <div class="menu">
              <%@include file="menungang.jsp" %>
            </div>
                
            <!--menu-->
            <br>
            <div class="content">
                <div id="title">
                                 >> Chi Tiết Sản Phẩm 
                             </div>
                <br>
               <div class="row">
                   
									
	                                  <jsp:useBean id="_sanpham" class="DAO.SanphamDAO" scope="page"/>          
                                                            <%  
                                                            
                                                                    Integer mssp = Integer.parseInt(request.getParameter("masp"));
                                                                        if(mssp != null)
                                                                         {
                                                                        Sanpham c = _sanpham.getSPByid(mssp);
                                                                        %>
                    <div class="col-xs-12 col-sm-6 col-md-8">
                    <div class="row">
                    	<div class="col-md-6">
                        <div id="hasp">
                            <img src="<%=c.getHinhAnh()%>" style="height:300px;width:250px;margin-left:50px;margin-top:20px;">
                        </div>
                        </div>
                        <div class="col-md-6">
                         <div id="title">
                                <%=c.getTenSp()%>
                             </div>
                <br>
                <%=c.getMieuTa()%>
                <br><br>
                Giá : <%=c.getGia()%> VNĐ
                <br><br><br>
                Số Lượng: <input type="text" id="soluong" name="soluong" value="0">
                <br><br>
               <button type="button" class="btn btn-danger" id="themgiohang">Thêm vào giỏ hàng</button>
                    <script>
                    $( "#themgiohang" ).click(function() {
                        
                        var sl =  $( "#soluong" ).val();
                        if($( "#soluong" ).val()<1)
                            alert("Bạn chưa nhập số lượng");
                        else
                                         $.ajax({
                        type: "POST",
                        url: "ThemvaogiohangController",
                        data: "massp=<%=c.getMaSp()%>&sl="+sl,
                        dataType: "text",
                        cache: false,
                        timeout: 60,
                        success: function(data) {
                            alert(data);
                        }});
                      });

                    </script>
                
                        </div>
                    </div><!--row con-->
                    </div>
                    <% }%>
                     <div class="col-xs-6 col-md-4">
                       <div class="panel panel-default">
                        <div class="panel-heading">
                             <h3 class="panel-title"><div id="title">Tin nổi bật</div></h3>
                        </div>
                        <div class="panel-body">
                            <h4 class="media-heading"><a href="">Cà phê và 11 điều có thể bạn chưa biết
                            <br/><small>26/05/2014 - Đặng Quốc Đạt</small></a></h4>
                            <p>Đã bao giờ bạn bơi trong một bồn cà phê chưa? Hay đã có lần nào bạn nghĩ tới chuyện uống cà phê bằng… đĩa? Nếu chưa thì hãy mở mang tầm nhìn với những điều mới mẻ về cà phê cùng</p>
                            <h4 class="media-heading"><a href="">Con đường cà phê - Cà phê cội
			    <br/><small>26/05/2014 - Đặng Quốc Đạt</small></a></h4>
			    <p>Nằm ở độ cao từ 400 tới 500m so với mực nước biển, với lợi thế là vùng đất đỏ bazan màu mở và vùng khí hậu thích hợp, Buôn Ma Thuộc – tỉnh Đắk Lắk từ lâu được biết đến như là thủ phủ cà phê Robusta của Việt Nam </p>
                        </div>
                       </div>
                       <div class="panel panel-default">
                         <div class="panel-heading"><div id="title">Liên Kiết</div></div>
                          <div class="panel-body">
                            Like Facebook
                          </div>
                          </div>
                        
                      </div>
                  </div>
</div><!--row-->
                
            <!--content-->
            <br>
            <div class="footer">
                <div class="container">
                    <h3><label class="label label-success">SWEET COFFEE SHOP</label></h3>
                </div>
            </div>
            <!--footer-->
        </div>
        <script type="text/javascript" src="script/jquery-min.js"></script>
    </body>
</html>

