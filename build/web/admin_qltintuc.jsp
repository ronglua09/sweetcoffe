<%-- 
    Document   : admin
    Created on : Jun 4, 2014, 11:16:07 PM
    Author     : QUOCDAT_HT
--%>

<%@page import="java.util.List"%>
<%@page import="Entities.Tintuc"%>
<%@page import="DAO.tintucDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>admin</title>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
         <link href="style/style_admin.css" rel="stylesheet" />
         <link href="style/stylesheet.css" rel="stylesheet" />
         <link href="style/style_ad.css" rel="stylesheet" />
         
   <script src="script/jquery.js" type="text/javascript"></script>
  
  
    </head>
    
    <body>
        <div class="row">
  <div class="col-md-9 col-md-push-3">
  
      <!--nội dung quan li tin tuc-->
        <div id="content" style="width: 1000px;">
  <div class="breadcrumb">
        <a href="#">Trang chủ</a>
         :: <a href="#" class="current">Tin tức</a>
      </div>
      <div class="box">
    <div class="heading">
      <h1><img src="img/news.png" alt=""> Tin tức</h1>
      <div class="buttons"><a id="btnthem" class="button">Thêm</a><a onclick="$('#form').attr('action', '#'); $('#form').submit();" class="button">Sao chép</a><a onclick="$('form').submit();" class="button">Xóa</a></div>
    </div>
    <div class="content">
      
        <table class="list">
          <thead>
            <tr>
                <td class="center">Mã Tin Tức</td>
              <td class="center">Tiêu Đề</td>    
              <td class="center">Phần Mở Đầu</td>
              <td class="center">Hình Ảnh</td>
              <td class="center">Loại Tin Tức</td>
              <td class="center">Thao Tác</td>
            </tr>
          </thead>
          <tbody>
              <%
                  List<Tintuc> _tintuc= new  tintucDAO().getAlltintuc();
                   for(Tintuc a:_tintuc)
                   {
                       
              %>
            <tr>
              <td class="center"><%=a.getMaTt()%></td>
              <td style="text-align: center;"><%=a.getTieuDe()%></td>
              <td class="center"><%=a.getModau()%></td>
              <td class="center">
                  <img height="40px" width="40px" src="<%=a.getHinhAnh()%>"/>
              </td>
              <td class="right"><%=a.getLoaitt().getTenLoaiTt()%></td>
              <td class=""> [ <a href="#" class="current">Sửa</a> 
                  
                 
                  <a href="TintucController?congviec=xoa&idxoa=<%=a.getMaTt()%>"><button> Xóa </button></a>
                </td>
            </tr>
                      
           
            
           <%
                   }
           %>
        </table>
      
    </div>
  </div>
</div>
        <!-- ket thuc noidung quan li tin tuc-->
  
  </div>
  <div class="col-md-3 col-md-pull-9" style="background: #6a6b72;color: #ffffff;border-right: 5px solid #269abc;height: 900px;">
      <!--menu-->
      <div id="tit">
          <h3>SweetCoffee</h3>
          Bảng quản trị<br>
          <h6>Xin chào,admin</h6>
          <h6>Xem trang web | Thoát</h6>
      </div>
      <div id='cssmenu'>
<ul>
    <li class='active'><a href="admin.jsp"><span>Bảng quản trị</span></a></li>
   <li class='has-sub'><a href='#'><span>Quản lí sản phẩm</span></a>
      <ul>
         <li><a href='#'><span>Menu sản phẩm</span></a></li>
         <li><a href='#'><span>Sản phẩm chi tiết</span></a></li>    
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí bán hàng</span></a>
      <ul>
         <li><a href='#'><span>Đơn đặt hàng</span></a></li>
         <li><a href='#'><span>Khách hàng</span></a></li>
          <li><a href='#'><span>Khuyến mãi</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí tin tức</span></a>
      <ul>
         <li><a href='#'><span>Tin tức cà phê</span></a></li>
         <li><a href='#'><span>Tin hoạt động</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí blog, hình ảnh</span></a>
      <ul>
         <li><a href='#'><span>Quản lí Blog</span></a></li>
         <li><a href='#'><span>Hình ảnh lưu niệm</span></a></li>
         <li><a href='#'><span>Hình ảnh slide</span></a></li>
      </ul>
   </li>
    <li class='has-sub'><a href='#'><span>Báo cáo , thống kê</span></a>
      <ul>
         <li><a href='#'><span>Chưa biết</span></a></li>
      </ul>
   </li>
</ul>
</div>
      
  </div>
</div>
        
                           
         <div class="footer">
            <div class="menu">
                <div class="container">
                    <br>
                        Contact| Sweet_Coffee @ 2014
                </div></div>
        </div>
        
    </body>
    <script src="script/js_admin.js" type="text/javascript"></script>
  
    
  
</html>
