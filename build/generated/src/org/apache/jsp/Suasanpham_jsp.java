package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import Entities.*;
import DAO.*;

public final class Suasanpham_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>admin</title>\n");
      out.write("         <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("         <link href=\"libs/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\"/>\n");
      out.write("         <link href=\"style/style_admin.css\" rel=\"stylesheet\" />\n");
      out.write("         <link href=\"style/style_ad.css\" rel=\"stylesheet\" />\n");
      out.write("          <link href=\"style/stylesheet.css\" rel=\"stylesheet\" />\n");
      out.write("   <script src=\"script/jquery.js\" type=\"text/javascript\"></script>\n");
      out.write("   <script src=\"ckeditor/ckeditor.js\" type=\"text/javascript\"></script>\n");
      out.write("   <script src=\"ckfinder/ckfinder.js\" type=\"text/javascript\"></script>\n");
      out.write("   \n");
      out.write("    </head\n");
      out.write("     <body>\n");
      out.write("       \n");
      out.write("          ");

       
       Nguoidung acc = null;
       try{
           
       if("logout".equals(request.getParameter("action")))
       {
           session.removeAttribute("user");
       }    
       
       acc = (Nguoidung) session.getAttribute("user");
       }catch(Exception e){}
       
       if(acc == null)
       {
           
      out.write("\n");
      out.write("           <form action=\"NguoidungController\" method=\"post\" id=\"dangnhap\">\n");
      out.write("               <aside style=\"height: 700px;background:url(img/bg-login.gif);color: #ffffff;\">\n");
      out.write("               <div class=\"user\" style=\"height: 200px;background: #101010;\">\n");
      out.write("                   <div id=\"top\" style=\"margin-left: 500px;\">\n");
      out.write("                       <img alt=\"Admin\" src=\"img/logotrong.png\"/><br><br>\n");
      out.write("    \n");
      out.write("      ĐĂNG NHẬP VÀO HỆ THỐNG\n");
      out.write("    </p>\n");
      out.write("                   </div>\n");
      out.write("               </div><br>\n");
      out.write("    \n");
      out.write("                   <div id=\"main-nav\" style=\"margin-left: 500px\">\n");
      out.write("                       <div id=\"left\" style=\"float: left\">\n");
      out.write("                           Tên đăng nhập :  &nbsp; &nbsp; &nbsp;<br><br>\n");
      out.write("                           Mật khẩu :  &nbsp; &nbsp; &nbsp; &nbsp; <br><br>\n");
      out.write("                       </div>\n");
      out.write("                       <div id=\"right\">\n");
      out.write("                              <input type=\"text\" id=\"ten\" name=\"ten\" /><br><br>\n");
      out.write("                                <input type=\"password\" id=\"pass\" name=\"pass\" /><br><br>\n");
      out.write("                       </div>\n");
      out.write("                       <a>\n");
      out.write("                                <input type=\"submit\" id=\"dangnhap\" name=\"dangnhap\" style=\"width: 100px;margin-left: 50px;\" /></a></li>\n");
      out.write("       \n");
      out.write("    </div>\n");
      out.write("         \n");
      out.write("</aside>\n");
      out.write("           \n");
      out.write("           </form>\n");
      out.write("           ");

       }
       else
       {
    
      out.write("\n");
      out.write("      \n");
      out.write("      <div class=\"row\">\n");
      out.write("  <div class=\"col-md-9 col-md-push-3\">\n");
      out.write("      \n");
      out.write("      <br><br>\n");
      out.write("        <h1>SỬA SẢN PHẨM</h1>\n");
      out.write("        \n");
      out.write("     \n");
      out.write("  </div>\n");
      out.write("  \n");
      out.write("    \n");
      out.write("                                                                        \n");
      out.write("    <div class=\"col-md-3 col-md-pull-9\" style=\"background: #6a6b72;color: #ffffff;border-right: 5px solid #269abc;height: 900px;\">\n");
      out.write("      <!--menu-->\n");
      out.write("      <div id=\"tit\">\n");
      out.write("          <h3>SweetCoffee</h3>\n");
      out.write("          Bảng quản trị<br>\n");
      out.write("          <h6>Xin chào,admin</h6>\n");
      out.write("          <h6>Xem trang web | Thoát</h6>\n");
      out.write("      </div>\n");
      out.write("      <div id='cssmenu'>\n");
      out.write("<ul>\n");
      out.write("    <li class='active'><a href=\"Sanpham.jsp\"><span>Bảng quản trị</span></a></li>\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí sản phẩm</span></a>\n");
      out.write("      <ul>\n");
      out.write("         <li><a href='#'><span>Menu sản phẩm</span></a></li>\n");
      out.write("         <li><a href='#'><span>Sản phẩm chi tiết</span></a></li>    \n");
      out.write("      </ul>\n");
      out.write("   </li>\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí bán hàng</span></a>\n");
      out.write("      <ul>\n");
      out.write("         <li><a href='#'><span>Đơn đặt hàng</span></a></li>\n");
      out.write("         <li><a href='#'><span>Khách hàng</span></a></li>\n");
      out.write("          <li><a href='#'><span>Khuyến mãi</span></a></li>\n");
      out.write("      </ul>\n");
      out.write("   </li>\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí tin tức</span></a>\n");
      out.write("      <ul>\n");
      out.write("         <li><a href='#'><span>Tin tức cà phê</span></a></li>\n");
      out.write("         <li><a href='#'><span>Tin hoạt động</span></a></li>\n");
      out.write("      </ul>\n");
      out.write("   </li>\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí blog, hình ảnh</span></a>\n");
      out.write("      <ul>\n");
      out.write("         <li><a href='#'><span>Quản lí Blog</span></a></li>\n");
      out.write("         <li><a href='#'><span>Hình ảnh lưu niệm</span></a></li>\n");
      out.write("         <li><a href='#'><span>Hình ảnh slide</span></a></li>\n");
      out.write("      </ul>\n");
      out.write("   </li>\n");
      out.write("    <li class='has-sub'><a href='#'><span>Báo cáo , thống kê</span></a>\n");
      out.write("      <ul>\n");
      out.write("         <li><a href='#'><span>Chưa biết</span></a></li>\n");
      out.write("      </ul>\n");
      out.write("   </li>\n");
      out.write("</ul>\n");
      out.write("</div>\n");
      out.write("      \n");
      out.write("  </div>\n");
      out.write("</div>\n");
      out.write("        <!--nội dung-->\n");
      out.write("        \n");
      out.write("        <!-- ket thuc noidung -->\n");
      out.write("         <div class=\"footer\">\n");
      out.write("            <div class=\"menu\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <br>\n");
      out.write("                        Contact| Sweet_Coffee @ 2014\n");
      out.write("                </div></div>\n");
      out.write("        </div>\n");
      out.write("        ");
}
      out.write("\n");
      out.write("    </body>\n");
      out.write("    <script src=\"script/js_admin.js\" type=\"text/javascript\"></script>\n");
      out.write("    <script type=\"text/javascript\">\n");
      out.write("        function BrowseServer() {\n");
      out.write("            var finder = new CKFinder();\n");
      out.write("            //finder.basePath = '../';\n");
      out.write("            finder.selectActionFunction = SetFileField;\n");
      out.write("            finder.popup();\n");
      out.write("        }\n");
      out.write("        function SetFileField(fileUrl) {\n");
      out.write("            document.getElementById('Image').value = fileUrl;\n");
      out.write("        }\n");
      out.write("    </script>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
