package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class tsp_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <script src=\"script/jquery.js\" type=\"text/javascript\"></script>\n");
      out.write("   <script src=\"ckeditor/ckeditor.js\" type=\"text/javascript\"></script>\n");
      out.write("   <script src=\"ckfinder/ckfinder.js\" type=\"text/javascript\"></script>\n");
      out.write("   \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("         <form name=\"f1\"  method=\"POST\"  class=\"\" enctype=\"multipart/form-data\" action=\"testtsp\" >\n");
      out.write("                    <div>\n");
      out.write("                        <span> Tên Sản Phẩm:  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>\n");
      out.write("                       <input type=\"text\" name=\"txttensanpham\" maxlength=\"255\" size=\"40\" placeholder=\"Nhập tên sản phẩm !\" required /><br />\n");
      out.write("                    </div><br>\n");
      out.write("                    <div>\n");
      out.write("                        \n");
      out.write("                      <span> Mã Loại Sản Phẩm: &nbsp; &nbsp;</span>\n");
      out.write("                       <input type=\"text\" name=\"txtloaisp\" maxlength=\"255\" size=\"40\"/><br />\n");
      out.write("                    </div>\n");
      out.write("            <div>\n");
      out.write("      <span><br />Hình Ảnh: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>\n");
      out.write("    <input type=\"text\" name=\"txthinhanh\" size=\"40\"/><br />\n");
      out.write("                    </div>\n");
      out.write("            <div>\n");
      out.write("                        \n");
      out.write("                      <span> Miêu tả :</span>\n");
      out.write("                      <textarea name=\"txtmieuta\"/></textarea>\n");
      out.write("            <br />\n");
      out.write("                    </div>\n");
      out.write("                     <div>\n");
      out.write("                        <span> Giá:</span>\n");
      out.write("                       <input type=\"text\" name=\"txtgia\" size=\"40\" /><br />\n");
      out.write("                    </div>\n");
      out.write("                    <br />\n");
      out.write("                        <input type=\"submit\" value=\"Lưu\" /><input type=\"reset\" value=\"Nhập lại\" />\n");
      out.write("                   \n");
      out.write("                </form> \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
