package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Entities.*;
import java.util.*;
import DAO.*;

public final class menu_005f1_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Menu</title>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width\">\n");
      out.write("        <!-- bootstrap -->\n");
      out.write("        <link href=\"libs/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\"/>\n");
      out.write("   <link href=\"style/style.css\" rel=\"stylesheet\" />\n");
      out.write("   <link href=\"style/menu_style.css\" rel=\"stylesheet\" />\n");
      out.write("   <script src=\"script/jquery.js\" type=\"text/javascript\"></script>\n");
      out.write("   <script src=\"script/script.js\" type=\"text/javascript\"></script>\n");
      out.write("   <script language=\"javascript\">\n");
      out.write("function chitiet(i)\n");
      out.write("{\n");
      out.write("document.forms[i].submit();\n");
      out.write("}\n");
      out.write("</script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"header\">\n");
      out.write("                \n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-6\"><a href=\"index.jsp\">\n");
      out.write("                            <img src=\"img/logotrong.png\"></a>\n");
      out.write("                             <div id=\"title\">\n");
      out.write("                                 Chút ngọt cho ly cà phê đắng!\n");
      out.write("                             </div>\n");
      out.write("                             \n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-md-6\">\n");
      out.write("                        <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("                            <li><a href=\"#\">Hướng Dẫn Mua Hàng</a></li>\n");
      out.write("                            <li><a href=\"#\">Thông Tin Vận Chuyển</a></li>\n");
      out.write("                            <li><a href=\"#\">Đăng Nhập</a></li>\n");
      out.write("                            <li><a href=\"#\"><span class=\"glyphicon glyphicon-shopping-cart\"></span> Giỏ hàng</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                        <form class=\"navbar-form navbar-left pull-right\" role=\"search\">\n");
      out.write("                            <div class=\"form-group\">\n");
      out.write("                                <input type=\"text\" class=\"form-control\" placeholder=\"Nhập tên sản phẩm\">\n");
      out.write("                            </div>\n");
      out.write("                            <button type=\"submit\" class=\"btn btn-default\">Tìm kiếm</button>\n");
      out.write("                        </form> \n");
      out.write("                        \n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                </div>\n");
      out.write("            \n");
      out.write("            <!--header-->\n");
      out.write("            <br>\n");
      out.write("            <div class=\"menu\">\n");
      out.write("               <ul class=\"nav nav-pills\">\n");
      out.write("                   <li><a href=\"trangchu.jsp\">Home</a></li>\n");
      out.write("                   <li><a href=\"menu.jsp\">Menu</a></li>\n");
      out.write("                     <li><a href=\"#\">Albums</a></li>\n");
      out.write("                     <li><a href=\"blogs.jsp\">Blogs</a></li>\n");
      out.write("                     <li><a href=\"danhsachtintuc.jsp\">Tin Tức</a></li>\n");
      out.write("                       <li><a href=\"#\">Liên Hệ</a></li>\n");
      out.write("   \n");
      out.write("</ul>\n");
      out.write(" \n");
      out.write("            </div>\n");
      out.write("                \n");
      out.write("            <!--menu-->\n");
      out.write("            <br>\n");
      out.write("            <div class=\"content\">\n");
      out.write("                <div id=\"title\">\n");
      out.write("                                 >> Menu Sản Phẩm\n");
      out.write("                             </div>\n");
      out.write("                <br>\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-xs-12 col-sm-6 col-md-8\">\n");
      out.write("                    ");
      DAO.LoaispDAO _loaisp = null;
      synchronized (_jspx_page_context) {
        _loaisp = (DAO.LoaispDAO) _jspx_page_context.getAttribute("_loaisp", PageContext.PAGE_SCOPE);
        if (_loaisp == null){
          _loaisp = new DAO.LoaispDAO();
          _jspx_page_context.setAttribute("_loaisp", _loaisp, PageContext.PAGE_SCOPE);
        }
      }
      out.write("          \n");
      out.write("                                                            ");
  List<Loaisp> _list =_loaisp.getAll();
                                                            int Stt = 1;
                                                            for(Loaisp c:_list )
                                                              {  
                                                                if(c.getMaLoaiSp() > 0)
                                                                 {
                                                                     Object obj=session.getAttribute("danhsachxem");
                                                                      ArrayList<String> ds = null;
                                                                        if(obj!=null)
                                                                        {
                                                                          ds = (ArrayList<String>) obj;
                                                                        }
                                                                 
      out.write("\n");
      out.write("                    \n");
      out.write("                        <div class=\"col-md-3\">\n");
      out.write("                            <form action=\"SanphamController\" method=\"POST\">\n");
      out.write("                                      <input type=\"hidden\" name=\"getMaLoaisp\" value=\"");
      out.print(c.getMaLoaiSp());
      out.write("\"/>\n");
      out.write("                            \n");
      out.write("                                    <img onmouseover=\"this.style.opacity=1;this.filters.alpha.opacity=100\"\n");
      out.write("                                         onmouseout=\"this.style.opacity=0.4;this.filters.alpha.opacity=40\" style=\"opacity:0.4;filter:alpha(opacity=40)\" src=\"");
      out.print(c.getHinhanh());
      out.write("\" onclick=\"chitiet(");
      out.print(Stt);
      out.write(");\">\n");
      out.write("                           \n");
      out.write("                            </form>\n");
      out.write("                    </div>\n");
      out.write("               \n");
      out.write("                            \n");
      out.write("                            ");

                                                                  Stt++;}
          }
          
      out.write("</div><!--product-->\n");
      out.write("                  <!--col-xs-12 col-sm-6 col-md-8-->\n");
      out.write("                  <div class=\"col-xs-6 col-md-4\">\n");
      out.write("                       <div class=\"panel panel-default\">\n");
      out.write("      <div class=\"panel-heading\">\n");
      out.write("          <h3 class=\"panel-title\"><div id=\"title\">Tin nổi bật</div></h3>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"panel-body\">\n");
      out.write("      \n");
      out.write("     <h4 class=\"media-heading\"><a href=\"\">Cà phê và 11 điều có thể bạn chưa biết\n");
      out.write("\t\t\t\t\t\t\t<br/><small>26/05/2014 - Đặng Quốc Đạt</small></a></h4>\n");
      out.write("\t\t\t\t\t\t<p>Đã bao giờ bạn bơi trong một bồn cà phê chưa? Hay đã có lần nào bạn nghĩ tới chuyện uống cà phê bằng… đĩa? Nếu chưa thì hãy mở mang tầm nhìn với những điều mới mẻ về cà phê cùng</p>\n");
      out.write("       <h4 class=\"media-heading\"><a href=\"\">Con đường cà phê - Cà phê cội\n");
      out.write("\t\t\t\t\t\t\t<br/><small>26/05/2014 - Đặng Quốc Đạt</small></a></h4>\n");
      out.write("\t\t\t\t\t\t<p>Nằm ở độ cao từ 400 tới 500m so với mực nước biển, với lợi thế là vùng đất đỏ bazan màu mở và vùng khí hậu thích hợp, Buôn Ma Thuộc – tỉnh Đắk Lắk từ lâu được biết đến như là thủ phủ cà phê Robusta của Việt Nam </p>\n");
      out.write("\n");
      out.write("      </div>\n");
      out.write("     \n");
      out.write("      \n");
      out.write("</div>\n");
      out.write("                       <div class=\"panel panel-default\">\n");
      out.write("                           <div class=\"panel-heading\"><div id=\"title\">Liên Kiết</div></div>\n");
      out.write("                          <div class=\"panel-body\">\n");
      out.write("                            Like Facebook\n");
      out.write("</div>\n");
      out.write("                          </div>\n");
      out.write("                        \n");
      out.write("                      </div>\n");
      out.write("                  </div>\n");
      out.write("</div><!--row-->\n");
      out.write("                \n");
      out.write("            <!--content-->\n");
      out.write("            <br>\n");
      out.write("            <div class=\"footer\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <h3><label class=\"label label-success\">SWEET COFFEE SHOP</label></h3>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <!--footer-->\n");
      out.write("        </div>\n");
      out.write("        <script type=\"text/javascript\" src=\"script/jquery-min.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
