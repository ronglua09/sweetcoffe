package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import Entities.Tintuc;
import Entities.Nguoidung;

public final class danhsachtintuc_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/header.jsp");
    _jspx_dependants.add("/menungang.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <title>SWEET COFFEE</title>\r\n");
      out.write("        <meta charset=\"UTF-8\">\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width\">\r\n");
      out.write("        <!-- bootstrap -->\r\n");
      out.write("        <link href=\"libs/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\"/>\r\n");
      out.write("   <link href=\"style/style.css\" rel=\"stylesheet\" />\r\n");
      out.write("   <script src=\"script/jquery.js\" type=\"text/javascript\"></script>\r\n");
      out.write("   <script src=\"script/script.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<script language=\"javascript\">\r\n");
      out.write("function chitiet(i)\r\n");
      out.write("{\r\n");
      out.write("document.forms[i].submit();\r\n");
      out.write("}\r\n");
      out.write("</script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <div class=\"container\">\r\n");
      out.write("            <div class=\"header\">\r\n");
      out.write("                \r\n");
      out.write("                 ");
      out.write("\r\n");
      out.write(" \r\n");
      out.write(" \r\n");
      out.write("\r\n");
      out.write("              <div class=\"row\">\r\n");
      out.write("                    <div class=\"col-md-6\"><a href=\"index.jsp\">\r\n");
      out.write("                            <img src=\"img/logotrong.png\"></a>\r\n");
      out.write("                             <div id=\"title\">\r\n");
      out.write("                                 Chút ngọt cho ly cà phê đắng!\r\n");
      out.write("                             </div>\r\n");
      out.write("                             \r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"col-md-6\">\r\n");
      out.write("                        <ul class=\"nav navbar-nav navbar-right\">\r\n");
      out.write("                            <li><a href=\"#\">Hướng Dẫn Mua Hàng</a></li>\r\n");
      out.write("                            <li><a href=\"#\">Thông Tin Vận Chuyển</a></li>  ");

       
       Nguoidung acc = null;
       try{
           
       if("logout".equals(request.getParameter("action")))
       {
           session.removeAttribute("users");
       }    
       
       acc = (Nguoidung) session.getAttribute("users");
       }catch(Exception e){}
       
       if(acc == null)
       {
           
      out.write("\r\n");
      out.write("                            <li><a href=\"Dangnhap.jsp\">Đăng Nhập</a></li>\r\n");
      out.write("                            ");
}else{
      out.write("\r\n");
      out.write("                            <li><a href=\"#\">");
      out.print(acc.getUsername());
      out.write("</a></li> ");

                                                                  
      out.write("\r\n");
      out.write("                            ");
}
      out.write("\r\n");
      out.write("                            <li><a href=\"#\"><span class=\"glyphicon glyphicon-shopping-cart\"></span> Giỏ hàng</a></li>\r\n");
      out.write("                        </ul>\r\n");
      out.write("                        <form class=\"navbar-form navbar-left pull-right\" role=\"search\">\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <input type=\"text\" class=\"form-control\" placeholder=\"Nhập tên sản phẩm\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <button type=\"submit\" class=\"btn btn-default\">Tìm kiếm</button>\r\n");
      out.write("                        </form> \r\n");
      out.write("                        \r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("           \r\n");
      out.write("  ");
      out.write("\r\n");
      out.write("                </div>\r\n");
      out.write("            \r\n");
      out.write("            <!--header-->\r\n");
      out.write("            <br>\r\n");
      out.write("            <div class=\"menu\">\r\n");
      out.write("               ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<ul class=\"nav nav-pills\">\r\n");
      out.write("                   <li><a href=\"trangchu.jsp\">Home</a></li>\r\n");
      out.write("                   <li><a href=\"menu.jsp\">Menu</a></li>\r\n");
      out.write("                     <li><a href=\"#\">Albums</a></li>\r\n");
      out.write("                     <li><a href=\"blogs.jsp\">Blogs</a></li>\r\n");
      out.write("                     <li><a href=\"danhsachtintuc.jsp\">Tin Tức</a></li>\r\n");
      out.write("                       <li><a href=\"#\">Liên Hệ</a></li>\r\n");
      out.write("   \r\n");
      out.write("</ul>\r\n");
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("                \r\n");
      out.write("\t\t\t<!-- end menu -->\r\n");
      out.write("\t\t\t<!-- content -->\r\n");
      out.write("\t\t\t<div class=\"content\" >\r\n");
      out.write("\t\t\t\t<div><p><h3><span class=\"glyphicon glyphicon-list\">\r\n");
      out.write("                            \r\n");
      out.write("\t\t\t\t\t\t\t</span>TIN TỨC</h3></p></div>\r\n");
      out.write("\t\t\t<hr>\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t\t<div class=\"row\">\r\n");
      out.write("\t\t\t\t\t<!-- Noi dung tin-->\r\n");
      out.write("\t\t\t\t\t<div class=\"col-md-8 mynew\">\r\n");
      out.write("\t\t\t\t\t\t<div class=\"row mytilte\"><div class=\"col-md-12\"><p><span class=\"glyphicon glyphicon-list-alt\"></span>Tin Tức Hoạt Động<p></div></div>\r\n");
      out.write("\t\t\t\t\t                                   ");
      DAO.tintucDAO _tintuc = null;
      synchronized (_jspx_page_context) {
        _tintuc = (DAO.tintucDAO) _jspx_page_context.getAttribute("_tintuc", PageContext.PAGE_SCOPE);
        if (_tintuc == null){
          _tintuc = new DAO.tintucDAO();
          _jspx_page_context.setAttribute("_tintuc", _tintuc, PageContext.PAGE_SCOPE);
        }
      }
      out.write("          \r\n");
      out.write("                                                                           ");
  
                                                                               String trang= "1";
                                                                               try{trang = session.getAttribute("sotrang").toString();}
                                                                               catch(Exception e){}
                                                                               //String trang = "1";
                                                                               if(trang==null || trang=="") 
                                                                                   trang="1";
                                                                       
                                                                List<Tintuc> _list =_tintuc.Gettintuctheotrang(Integer.parseInt(trang));
                                                                if(_list != null){
                                                            int Stt = 1;
                                                            for(Tintuc c:_list )
                                                              {  
                                                                if(c.getMaTt() > 0)
                                                                 {
                                                                     Object obj=session.getAttribute("danhsachxem");
                                                                      ArrayList<String> ds = null;
                                                                        if(obj!=null)
                                                                        {
                                                                          ds = (ArrayList<String>) obj;
                                                                        }
                                                                 
      out.write("\r\n");
      out.write("                                                                 <div class=\"row\">\r\n");
      out.write("                                                                 <div class=\"col-md-4 grow \">\r\n");
      out.write("                                                                     <form action=\"chitiettintucontrolller\" method=\"POST\">\r\n");
      out.write("                                                                     <input type=\"hidden\" name=\"getMaTt\" value=\"");
      out.print(c.getMaTt());
      out.write("\"/>\r\n");
      out.write("                                                                      <b>\r\n");
      out.write("                                                                           <img width='190px' height='170px' src=\"");
      out.print(c.getHinhAnh());
      out.write("\" onclick=\"chitiet(");
      out.print(Stt);
      out.write(");\">\r\n");
      out.write("                                                                        </b>\r\n");
      out.write("                                                                    </form>   \r\n");
      out.write("                                                            \r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-md-7 mysize\">\r\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"row\">\r\n");
      out.write("                                                                   \r\n");
      out.write("                                                                    <a href=\"#\" onclick=\"chitiet(");
      out.print(Stt);
      out.write(");\" ><h4>");
      out.print(c.getTieuDe());
      out.write("</h4></a>\r\n");
      out.write("                                                                        \r\n");
      out.write("                                                                       \r\n");
      out.write("                                                                    </form> \r\n");
      out.write("                                                                           \r\n");
      out.write("                                                                         \r\n");
      out.write("                                                                   \r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"row\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>");
      out.print(c.getModau());
      out.write("</p>\r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"row\">\r\n");
      out.write("                                                                   \r\n");
      out.write("                                                                    <a href=\"#\" onclick=\"chitiet(");
      out.print(Stt);
      out.write(");\"><p>Xem Thêm <span class=\"glyphicon glyphicon-circle-arrow-right\"></span></p></a>\r\n");
      out.write("                                                                       \r\n");
      out.write("                                                                          \r\n");
      out.write("                                                                    \r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("                                                        </div></div>\r\n");
      out.write("\t\t\t\t\t\t<!-- Ket thuc tin-->\t\t\t\t\t\t\r\n");
      out.write("                                                <hr>\r\n");
      out.write("                                       \r\n");
      out.write("\r\n");
      out.write("\t");

                                                                  Stt++;}}
          }
          
      out.write("\r\n");
      out.write("                                      \r\n");
      out.write("                                       \r\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\r\n");
      out.write("\t\t\t\t\t\t\t<center>\r\n");
      out.write("\t\t\t\t\t\t\t<ul class=\"pagination\">\r\n");
      out.write("\t\t\t\t\t\t\t  <li ><a href=\"#\">&laquo;</a></li>\r\n");
      out.write("                                                          ");

                                                          for(int i=1; i<=((_tintuc.getAlltintuc().size()/5)+1);i++)
                                                          {
                                                              if(i==Integer.parseInt(session.getAttribute("sotrang").toString())) // lay so trang hien tai o session
                                                              {
                                                                    
      out.write("\r\n");
      out.write("                                                                        <li class=\"active\"><a href=\"TintucController?trang=");
      out.print(i);
      out.write('"');
      out.write('>');
      out.print(i);
      out.write("</a></li>          \r\n");
      out.write("                                                                    ");
 
                                                              }
                                                              
                                                              else // khong phai so trang hien tai van in ra
                                                              {
                                                                    
      out.write("\r\n");
      out.write("                                                                        <li><a href=\"TintucController?trang=");
      out.print(i);
      out.write('"');
      out.write('>');
      out.print(i);
      out.write("</a></li>  \r\n");
      out.write("                                                                    ");

                                                              }
                                                            }
                                                          
      out.write("\r\n");
      out.write("\t\t\t\t\t\t\t  <li><a href=\"#\">&raquo;</a></li>\r\n");
      out.write("                                                          \r\n");
      out.write("\t\t\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t\t\t\t</center>\r\n");
      out.write("\t\t\t\t\t\t</div>\r\n");
      out.write("\r\n");
      out.write("                                                                 </div>\r\n");
      out.write("                                      \r\n");
      out.write("\t\t\t\t\t<!--Phan tin khuyen mai-->\r\n");
      out.write("\t\t\t\t\t<div class=\"col-md-4\">\r\n");
      out.write("\t\t\t\t\t\t<div class=\"row mytilte\"><div class=\"col-md-12\"><p><span class=\"glyphicon glyphicon-list-alt\"></span>Tin Tức Khuyến Mãi<p></div></div>\r\n");
      out.write("\t\t\t\t\t\t<!--chi tiet khuyen mai-->\r\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-md-12\">\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"thumbnail\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"img/1348068761-ca-phe-cappuccino.jpg\" alt=\"...\">\r\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"caption\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"tieude\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Caffe Capuchino</a>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t Cà phê pha chút ngọt ngào của vị CAPUCHINO\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"Subdrop2\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tCà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div> \r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t<!--ket thuc chi tiet khuyen mai-->\r\n");
      out.write("\t\t\t\t\t\t<!--chi tiet khuyen mai-->\r\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-md-12\">\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"thumbnail\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"img/1348068761-ca-phe-cappuccino.jpg\" alt=\"...\">\r\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"caption\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"tieude\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Caffe Capuchino</a>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t Cà phê pha chút ngọt ngào của vị CAPUCHINO\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"Subdrop2\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tCà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div> \r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t<!--ket thuc chi tiet khuyen mai-->\r\n");
      out.write("\t\t\t\t\t\t<!--chi tiet khuyen mai-->\r\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-md-12\">\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"thumbnail\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"img/1348068761-ca-phe-cappuccino.jpg\" alt=\"...\">\r\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"caption\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"tieude\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Caffe Capuchino</a>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t Cà phê pha chút ngọt ngào của vị CAPUCHINO\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"Subdrop2\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tCà phê pha chút ngọt ngào của vị CAPUCHINO.Capuccino kết hợp từ  1/3 Espresso, 1/3 sữa tươi nóng làm mềm vị đắng của cà phê và 1/3 bọt sữa mịn màng rắc bột quế hoặc chocolate vụn\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div> \r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t<!--ket thuc chi tiet khuyen mai-->\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t<!-- ket thuc tin khuyen mai-->\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("            <!-- end content-->\r\n");
      out.write("            <br>\r\n");
      out.write("            <div class=\"footer\">\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <h3><label class=\"label label-success\">SWEET COFFEE SHOP</label></h3>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <!--footer-->\r\n");
      out.write("        </div>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"script/jquery-min.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
