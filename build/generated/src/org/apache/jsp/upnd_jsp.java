package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class upnd_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <form name=\"taikhoan\"  method=\"POST\"  class=\"registrations\" action=\"themnd\" accept-charset=\"ISO-8859-1\" >\n");
      out.write("                <div class=\"contact\">\n");
      out.write("                    <div id=\"nhapdulieu\">\n");
      out.write("                        <h3>Để hoàn thành việc đăng kí, mời bạn nhập thông tin tài khoản người dùng để đăng nhập vào Website</h3>\n");
      out.write("                        <div id=\"kh_left\">\n");
      out.write("                            <br>\n");
      out.write("                           \n");
      out.write("                            <br><br><br>\n");
      out.write("                            Tên tài khoản :\n");
      out.write("                            <br><br><br>\n");
      out.write("                            Mật Khẩu:\n");
      out.write("                            <br><br><br>\n");
      out.write("                          \n");
      out.write("                        </div>\n");
      out.write("                        <div id=\"kh_right\">\n");
      out.write("                            \n");
      out.write("                             <br>\n");
      out.write("                             <br><br><br>\n");
      out.write("                           <input type=\"text\" class=\"form-control\" id=\"username\" name=\"username\" placeholder=\"Nhập tên tài khoản\" style=\"width: 300px;\">\n");
      out.write("                            <br>\n");
      out.write("                             <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" placeholder=\"Thông tin bắt buộc*\" style=\"width: 300px;\">\n");
      out.write("                           <br>\n");
      out.write("                          \n");
      out.write("                        </div>\n");
      out.write("                        <br><br><br>\n");
      out.write("                        <input type=\"submit\" value=\"Đồng Ý\" style=\"margin-left: 20px;\"/>\n");
      out.write("                    </div>\n");
      out.write("                 \n");
      out.write("                    \n");
      out.write("                </div>\n");
      out.write("                </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
