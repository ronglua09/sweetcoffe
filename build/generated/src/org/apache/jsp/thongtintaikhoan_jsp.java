package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Entities.Nguoidung;

public final class thongtintaikhoan_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/header.jsp");
    _jspx_dependants.add("/menungang.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Menu chi tiet</title>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width\">\n");
      out.write("        <!-- bootstrap -->\n");
      out.write("        <link href=\"libs/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\"/>\n");
      out.write("   <link href=\"style/style.css\" rel=\"stylesheet\" />\n");
      out.write("   <link href=\"style/menu_style.css\" rel=\"stylesheet\" />\n");
      out.write("   <script src=\"script/jquery.js\" type=\"text/javascript\"></script>\n");
      out.write("   <script src=\"script/script.js\" type=\"text/javascript\"></script>\n");
      out.write("   \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"header\">\n");
      out.write("                 ");
      out.write("\n");
      out.write(" \n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-6\">\n");
      out.write("                        <a href=\"index.jsp\">\n");
      out.write("                            <img src=\"img/logotrong.png\"></a>\n");
      out.write("                             <div id=\"title\">\n");
      out.write("                                 Chút ngọt cho ly cà phê đắng!\n");
      out.write("                             </div>\n");
      out.write("                             \n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-md-6\">\n");
      out.write("                        <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("                            <li><a href=\"#\">Hướng Dẫn Mua Hàng</a></li>\n");
      out.write("                            <li><a href=\"#\">Thông Tin Vận Chuyển</a></li>\n");
      out.write("                             ");

       
       Nguoidung acc = null;
       try{
           
       if("logout".equals(request.getParameter("action")))
       {
           session.removeAttribute("users");
       }    
       
       acc = (Nguoidung) session.getAttribute("users");
       }catch(Exception e){}
       
       if(acc == null)
       {
           
      out.write("\n");
      out.write("                            <li><a href=\"Dangnhap.jsp\">Đăng Nhập</a></li>\n");
      out.write("                            ");
}else{
      out.write("\n");
      out.write("                            \n");
      out.write("                            <li><a href=\"#\">");
      out.print(acc.getUsername());
      out.write("</a></li>\n");
      out.write("                            ");
}
      out.write("\n");
      out.write("                            <li><a href=\"#\"><span class=\"glyphicon glyphicon-shopping-cart\"></span> Giỏ hàng</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                        <form class=\"navbar-form navbar-left pull-right\" role=\"search\">\n");
      out.write("                            <div class=\"form-group\">\n");
      out.write("                                <input type=\"text\" class=\"form-control\" placeholder=\"Nhập tên sản phẩm\">\n");
      out.write("                            </div>\n");
      out.write("                            <button type=\"submit\" class=\"btn btn-default\">Tìm kiếm</button>\n");
      out.write("                        </form> \n");
      out.write("                        \n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("           \n");
      out.write("  ");
      out.write("\n");
      out.write("            </div>\n");
      out.write("            <!--header-->\n");
      out.write("            <br>\n");
      out.write("            <div class=\"menu\">\n");
      out.write("               ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<ul class=\"nav nav-pills\">\n");
      out.write("                   <li><a href=\"trangchu.jsp\">Home</a></li>\n");
      out.write("                   <li><a href=\"menu.jsp\">Menu</a></li>\n");
      out.write("                     <li><a href=\"#\">Albums</a></li>\n");
      out.write("                     <li><a href=\"blogs.jsp\">Blogs</a></li>\n");
      out.write("                     <li><a href=\"danhsachtintuc.jsp\">Tin Tức</a></li>\n");
      out.write("                       <li><a href=\"#\">Liên Hệ</a></li>\n");
      out.write("   \n");
      out.write("</ul>\n");
      out.write("\n");
      out.write(" \n");
      out.write("            </div>\n");
      out.write("                \n");
      out.write("            <!--menu-->\n");
      out.write("            <br>\n");
      out.write("            <div class=\"content\">\n");
      out.write("                <div id=\"title\">\n");
      out.write("                                 >> Đăng ký tài khoản\n");
      out.write("                             </div>\n");
      out.write("                <br>\n");
      out.write("               <div class=\"contact\">\n");
      out.write("                    <div id=\"nhapdulieu\">\n");
      out.write("                        <div id=\"kh_left\">\n");
      out.write("                            <br>\n");
      out.write("                            Họ Tên: \n");
      out.write("                            <br><br><br>\n");
      out.write("                            Địa chỉ:\n");
      out.write("                            <br><br><br>\n");
      out.write("                             Giới tính:\n");
      out.write("                            <br><br><br>\n");
      out.write("                            Email :\n");
      out.write("                            <br><br>\n");
      out.write("                            Số Điên Thoại:\n");
      out.write("                            <br><br>\n");
      out.write("                           \n");
      out.write("                        </div>\n");
      out.write("                        <div id=\"kh_right\">\n");
      out.write("                            <input type=\"hidden\" class=\"form-control\" id=\"id\" name=\"id\">\n");
      out.write("                             <br>\n");
      out.write("                             <input type=\"text\" class=\"form-control\" id=\"hoten\" name=\"hoten\" placeholder=\"Nhập họ tên\" style=\"width: 300px;\"> \n");
      out.write("                            <br>\n");
      out.write("                            <select class=\"form-control\" name=\"diachi\" style=\"width: 300px;\" >\n");
      out.write("                                <option>Hồ Chí Minh</option>\n");
      out.write("                                <option>Khác</option>\n");
      out.write("                            </select>  <br>\n");
      out.write("                              <select class=\"form-control\" name=\"gioitinh\" style=\"width: 300px;\" >\n");
      out.write("                                <option>Nam</option>\n");
      out.write("                                <option>Nữ</option>\n");
      out.write("                            </select> <br>\n");
      out.write("                            <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" placeholder=\"Nhập Email\" style=\"width: 300px;\">\n");
      out.write("                           <br>\n");
      out.write("                           <input type=\"text\" class=\"form-control\" id=\"sdt\" name=\"sdt\" placeholder=\"Thông tin bắt buộc*\" style=\"width: 300px;\">\n");
      out.write("                          \n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write(" \n");
      out.write("            </div>\n");
      out.write("                \n");
      out.write("            <!--content-->\n");
      out.write("            <br>\n");
      out.write("            <div class=\"footer\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <h3><label class=\"label label-success\">SWEET COFFEE SHOP</label></h3>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <!--footer-->\n");
      out.write("        </div>\n");
      out.write("        <script type=\"text/javascript\" src=\"script/jquery-min.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
