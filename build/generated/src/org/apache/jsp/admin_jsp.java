package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import Entities.*;
import DAO.*;

public final class admin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>admin</title>\r\n");
      out.write("         <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("         <link href=\"libs/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\"/>\r\n");
      out.write("         <link href=\"style/style_admin.css\" rel=\"stylesheet\" />\r\n");
      out.write("         <link href=\"style/style_ad.css\" rel=\"stylesheet\" />\r\n");
      out.write("          <link href=\"style/stylesheet.css\" rel=\"stylesheet\" />\r\n");
      out.write("   <script src=\"script/jquery.js\" type=\"text/javascript\"></script>\r\n");
      out.write("  \r\n");
      out.write("  \r\n");
      out.write("    </head>\r\n");
      out.write("    \r\n");
      out.write("    <body>\r\n");
      out.write("       ");

       
       Nguoidung acc = null;
       try{
           
       if("logout".equals(request.getParameter("action")))
       {
           session.removeAttribute("user");
       }    
       
       acc = (Nguoidung) session.getAttribute("user");
       }catch(Exception e){}
       
       if(acc == null)
       {
           
      out.write("\r\n");
      out.write("           <form action=\"NguoidungController\" method=\"post\" id=\"dangnhap\">\r\n");
      out.write("               <aside style=\"height: 700px;background:url(img/bg-login.gif);color: #ffffff;\">\r\n");
      out.write("               <div class=\"user\" style=\"height: 200px;background: #101010;\">\r\n");
      out.write("                   <div id=\"top\" style=\"margin-left: 500px;\">\r\n");
      out.write("                       <img alt=\"Admin\" src=\"img/logotrong.png\"/><br><br>\r\n");
      out.write("    \r\n");
      out.write("      ĐĂNG NHẬP VÀO HỆ THỐNG\r\n");
      out.write("    </p>\r\n");
      out.write("                   </div>\r\n");
      out.write("               </div><br>\r\n");
      out.write("    \r\n");
      out.write("                   <div id=\"main-nav\" style=\"margin-left: 500px\">\r\n");
      out.write("                       <div id=\"left\" style=\"float: left;color: #ffffff\">\r\n");
      out.write("                           Tên đăng nhập :<br><br>\r\n");
      out.write("                           Mật khẩu :<br><br>\r\n");
      out.write("                       </div>\r\n");
      out.write("                       <div id=\"right\">\r\n");
      out.write("                              <input type=\"text\" id=\"ten\" name=\"ten\" /><br><br>\r\n");
      out.write("                                <input type=\"password\" id=\"pass\" name=\"pass\" /><br><br>\r\n");
      out.write("                       </div>\r\n");
      out.write("                       <a>\r\n");
      out.write("                                <input type=\"submit\" id=\"dangnhap\" name=\"dangnhap\" style=\"width: 100px;margin-left: 50px;\" /></a></li>\r\n");
      out.write("       \r\n");
      out.write("    </div>\r\n");
      out.write("         \r\n");
      out.write("</aside>\r\n");
      out.write("           \r\n");
      out.write("           </form>\r\n");
      out.write("           ");

       }
       else
       {
    
      out.write("\r\n");
      out.write("    <div class=\"row\">\r\n");
      out.write("  <div class=\"col-md-9 col-md-push-3\">\r\n");
      out.write("    \r\n");
      out.write("  </div><!--hết nội dung-->\r\n");
      out.write("  <div class=\"col-md-3 col-md-pull-9\" style=\"background: #6a6b72;color: #ffffff;border-right: 5px solid #269abc;height: 900px;\">\r\n");
      out.write("      <!--menu-->\r\n");
      out.write("      <div id=\"tit\">\r\n");
      out.write("          <h3>SweetCoffee</h3>\r\n");
      out.write("          Bảng quản trị<br>\r\n");
      out.write("          <h6>Xin chào,admin</h6>\r\n");
      out.write("          <h6>Xem trang web | Thoát</h6>\r\n");
      out.write("      </div>\r\n");
      out.write("      <div id='cssmenu'>\r\n");
      out.write("<ul>\r\n");
      out.write("    <li class='active'><a href=\"Sanpham.jsp\"><span>Bảng quản trị</span></a></li>\r\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí sản phẩm</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Menu sản phẩm</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Sản phẩm chi tiết</span></a></li>    \r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí bán hàng</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Đơn đặt hàng</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Khách hàng</span></a></li>\r\n");
      out.write("          <li><a href='#'><span>Khuyến mãi</span></a></li>\r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí tin tức</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Tin tức cà phê</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Tin hoạt động</span></a></li>\r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí blog, hình ảnh</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Quản lí Blog</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Hình ảnh lưu niệm</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Hình ảnh slide</span></a></li>\r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("    <li class='has-sub'><a href='#'><span>Báo cáo , thống kê</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Chưa biết</span></a></li>\r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("</ul>\r\n");
      out.write("</div>\r\n");
      out.write("      \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("        <!--nội dung-->\r\n");
      out.write("        \r\n");
      out.write("        <!-- ket thuc noidung -->\r\n");
      out.write("         <div class=\"footer\">\r\n");
      out.write("            <div class=\"menu\">\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <br>\r\n");
      out.write("                        Contact| Sweet_Coffee @ 2014\r\n");
      out.write("                </div></div>\r\n");
      out.write("        </div>\r\n");
      out.write("        ");
}
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("    <script src=\"script/js_admin.js\" type=\"text/javascript\"></script>\r\n");
      out.write("  \r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
