package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import Entities.*;
import DAO.*;

public final class chitietsanpham_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/header.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <title>Sản Phẩm</title>\r\n");
      out.write("        <meta charset=\"UTF-8\">\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width\">\r\n");
      out.write("        <!-- bootstrap -->\r\n");
      out.write("        <link href=\"libs/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\"/>\r\n");
      out.write("   <link href=\"style/style.css\" rel=\"stylesheet\" />\r\n");
      out.write("   <link href=\"style/menu_style.css\" rel=\"stylesheet\" />\r\n");
      out.write("   <script src=\"script/jquery.js\" type=\"text/javascript\"></script>\r\n");
      out.write("   <script src=\"script/script.js\" type=\"text/javascript\"></script>\r\n");
      out.write("   \r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <div class=\"container\">\r\n");
      out.write("            <div class=\"header\">\r\n");
      out.write("                \r\n");
      out.write("                 ");
      out.write(" \n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-6\">\n");
      out.write("                        <a href=\"index.jsp\">\n");
      out.write("                            <img src=\"img/logotrong.png\"></a>\n");
      out.write("                             <div id=\"title\">\n");
      out.write("                                 Chút ngọt cho ly cà phê đắng!\n");
      out.write("                             </div>\n");
      out.write("                             \n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-md-6\">\n");
      out.write("                        <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("                            <li><a href=\"#\">Hướng Dẫn Mua Hàng</a></li>\n");
      out.write("                            <li><a href=\"#\">Thông Tin Vận Chuyển</a></li>\n");
      out.write("                            <li><a href=\"#\">Đăng Nhập</a></li>\n");
      out.write("                            <li><a href=\"#\"><span class=\"glyphicon glyphicon-shopping-cart\"></span> Giỏ hàng</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                        <form class=\"navbar-form navbar-left pull-right\" role=\"search\">\n");
      out.write("                            <div class=\"form-group\">\n");
      out.write("                                <input type=\"text\" class=\"form-control\" placeholder=\"Nhập tên sản phẩm\">\n");
      out.write("                            </div>\n");
      out.write("                            <button type=\"submit\" class=\"btn btn-default\">Tìm kiếm</button>\n");
      out.write("                        </form> \n");
      out.write("                        \n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("           \n");
      out.write("  ");
      out.write("\r\n");
      out.write("                </div>\r\n");
      out.write("            \r\n");
      out.write("            <!--header-->\r\n");
      out.write("            <br>\r\n");
      out.write("            <div class=\"menu\">\r\n");
      out.write("               <ul class=\"nav nav-pills\">\r\n");
      out.write("                   <li><a href=\"trangchu.jsp\">Home</a></li>\r\n");
      out.write("                   <li><a href=\"menu.jsp\">Menu</a></li>\r\n");
      out.write("                     <li><a href=\"#\">Albums</a></li>\r\n");
      out.write("                     <li><a href=\"blogs.jsp\">Blogs</a></li>\r\n");
      out.write("                     <li><a href=\"danhsachtintuc.jsp\">Tin Tức</a></li>\r\n");
      out.write("                       <li><a href=\"#\">Liên Hệ</a></li>\r\n");
      out.write("   \r\n");
      out.write("</ul>\r\n");
      out.write(" \r\n");
      out.write("            </div>\r\n");
      out.write("                \r\n");
      out.write("            <!--menu-->\r\n");
      out.write("            <br>\r\n");
      out.write("            <div class=\"content\">\r\n");
      out.write("                <div id=\"title\">\r\n");
      out.write("                                 >> Chi Tiết Sản Phẩm \r\n");
      out.write("                             </div>\r\n");
      out.write("                <br>\r\n");
      out.write("               <div class=\"row\">\r\n");
      out.write("                   \r\n");
      out.write("\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t                                  ");
      DAO.SanphamDAO _sanpham = null;
      synchronized (_jspx_page_context) {
        _sanpham = (DAO.SanphamDAO) _jspx_page_context.getAttribute("_sanpham", PageContext.PAGE_SCOPE);
        if (_sanpham == null){
          _sanpham = new DAO.SanphamDAO();
          _jspx_page_context.setAttribute("_sanpham", _sanpham, PageContext.PAGE_SCOPE);
        }
      }
      out.write("          \r\n");
      out.write("                                                            ");
  
                                                            Object obj=session.getAttribute("danhsachxem");
                                                                if(obj!=null)
                                                                 {
                                                                 ArrayList<Integer> ds = (ArrayList<Integer>) obj;
                                                                    Integer mssp = 1;
                                                                    mssp = ds.get(ds.size()-1);
                                                                        if(mssp != null)
                                                                         {
                                                                        Sanpham c = _sanpham.getSPByid(mssp);
                                                                        
      out.write("\r\n");
      out.write("                    <div class=\"col-xs-12 col-sm-6 col-md-8\">\r\n");
      out.write("                    <div class=\"row\">\r\n");
      out.write("                    \t<div class=\"col-md-6\">\r\n");
      out.write("                        <div id=\"hasp\">\r\n");
      out.write("                            <img src=\"");
      out.print(c.getHinhAnh());
      out.write("\" style=\"height:300px;width:250px;margin-left:50px;margin-top:20px;\">\r\n");
      out.write("                        </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"col-md-6\">\r\n");
      out.write("                         <div id=\"title\">\r\n");
      out.write("                                ");
      out.print(c.getTenSp());
      out.write("\r\n");
      out.write("                             </div>\r\n");
      out.write("                <br>\r\n");
      out.write("                ");
      out.print(c.getMieuTa());
      out.write("\r\n");
      out.write("                <br><br>\r\n");
      out.write("                Giá : ");
      out.print(c.getGia());
      out.write(" VNĐ\r\n");
      out.write("                <br><br><br>\r\n");
      out.write("                Số Lượng: <input type=\"text\" id=\"soluong\" name=\"soluong\">\r\n");
      out.write("                <br><br>\r\n");
      out.write("               <button type=\"button\" class=\"btn btn-danger\">Thêm vào giỏ hàng</button>\r\n");
      out.write("                \r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div><!--row con-->\r\n");
      out.write("                    </div>\r\n");
      out.write("                    ");
 }}
      out.write("\r\n");
      out.write("                     <div class=\"col-xs-6 col-md-4\">\r\n");
      out.write("                       <div class=\"panel panel-default\">\r\n");
      out.write("                        <div class=\"panel-heading\">\r\n");
      out.write("                             <h3 class=\"panel-title\"><div id=\"title\">Tin nổi bật</div></h3>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"panel-body\">\r\n");
      out.write("                            <h4 class=\"media-heading\"><a href=\"\">Cà phê và 11 điều có thể bạn chưa biết\r\n");
      out.write("                            <br/><small>26/05/2014 - Đặng Quốc Đạt</small></a></h4>\r\n");
      out.write("                            <p>Đã bao giờ bạn bơi trong một bồn cà phê chưa? Hay đã có lần nào bạn nghĩ tới chuyện uống cà phê bằng… đĩa? Nếu chưa thì hãy mở mang tầm nhìn với những điều mới mẻ về cà phê cùng</p>\r\n");
      out.write("                            <h4 class=\"media-heading\"><a href=\"\">Con đường cà phê - Cà phê cội\r\n");
      out.write("\t\t\t    <br/><small>26/05/2014 - Đặng Quốc Đạt</small></a></h4>\r\n");
      out.write("\t\t\t    <p>Nằm ở độ cao từ 400 tới 500m so với mực nước biển, với lợi thế là vùng đất đỏ bazan màu mở và vùng khí hậu thích hợp, Buôn Ma Thuộc – tỉnh Đắk Lắk từ lâu được biết đến như là thủ phủ cà phê Robusta của Việt Nam </p>\r\n");
      out.write("                        </div>\r\n");
      out.write("                       </div>\r\n");
      out.write("                       <div class=\"panel panel-default\">\r\n");
      out.write("                         <div class=\"panel-heading\"><div id=\"title\">Liên Kiết</div></div>\r\n");
      out.write("                          <div class=\"panel-body\">\r\n");
      out.write("                            Like Facebook\r\n");
      out.write("                          </div>\r\n");
      out.write("                          </div>\r\n");
      out.write("                        \r\n");
      out.write("                      </div>\r\n");
      out.write("                  </div>\r\n");
      out.write("</div><!--row-->\r\n");
      out.write("                \r\n");
      out.write("            <!--content-->\r\n");
      out.write("            <br>\r\n");
      out.write("            <div class=\"footer\">\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <h3><label class=\"label label-success\">SWEET COFFEE SHOP</label></h3>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <!--footer-->\r\n");
      out.write("        </div>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"script/jquery-min.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
