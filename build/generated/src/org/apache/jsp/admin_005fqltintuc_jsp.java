package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;
import Entities.Tintuc;
import DAO.tintucDAO;

public final class admin_005fqltintuc_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>admin</title>\r\n");
      out.write("         <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("         <link href=\"libs/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\"/>\r\n");
      out.write("         <link href=\"style/style_admin.css\" rel=\"stylesheet\" />\r\n");
      out.write("         <link href=\"style/stylesheet.css\" rel=\"stylesheet\" />\r\n");
      out.write("         <link href=\"style/style_ad.css\" rel=\"stylesheet\" />\r\n");
      out.write("   <script src=\"script/jquery.js\" type=\"text/javascript\"></script>\r\n");
      out.write("  \r\n");
      out.write("  \r\n");
      out.write("    </head>\r\n");
      out.write("    \r\n");
      out.write("    <body>\r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("  <div class=\"col-md-9 col-md-push-3\">\r\n");
      out.write("  \r\n");
      out.write("      <!--nội dung quan li tin tuc-->\r\n");
      out.write("        <div id=\"content\" style=\"width: 1000px;\">\r\n");
      out.write("  <div class=\"breadcrumb\">\r\n");
      out.write("        <a href=\"#\">Trang chủ</a>\r\n");
      out.write("         :: <a href=\"#\" class=\"current\">Tin tức</a>\r\n");
      out.write("      </div>\r\n");
      out.write("      <div class=\"box\">\r\n");
      out.write("    <div class=\"heading\">\r\n");
      out.write("      <h1><img src=\"img/news.png\" alt=\"\"> Tin tức</h1>\r\n");
      out.write("      <div class=\"buttons\"><a onclick=\"location = '#'\" class=\"button\">Thêm</a><a onclick=\"$('#form').attr('action', '#'); $('#form').submit();\" class=\"button\">Sao chép</a><a onclick=\"$('form').submit();\" class=\"button\">Xóa</a></div>\r\n");
      out.write("    </div>\r\n");
      out.write("    <div class=\"content\">\r\n");
      out.write("      <form action=\"#\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\">\r\n");
      out.write("        <table class=\"list\">\r\n");
      out.write("          <thead>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"center\">Mã Tin Tức</td>\r\n");
      out.write("              <td class=\"center\">Tiêu Đề</td>    \r\n");
      out.write("              <td class=\"center\">Phần Mở Đầu</td>\r\n");
      out.write("              <td class=\"center\">Hình Ảnh</td>\r\n");
      out.write("              <td class=\"center\">Loại Tin Tức</td>\r\n");
      out.write("              <td class=\"center\">Thao Tác</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("          </thead>\r\n");
      out.write("          <tbody>\r\n");
      out.write("              ");

                  List<Tintuc> _tintuc= new  tintucDAO().getAlltintuc();
                   for(Tintuc a:_tintuc)
                   {
                       
              
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("              <td class=\"center\">");
      out.print(a.getMaTt());
      out.write("</td>\r\n");
      out.write("              <td style=\"text-align: center;\">");
      out.print(a.getTieuDe());
      out.write("</td>\r\n");
      out.write("              <td class=\"center\">");
      out.print(a.getModau());
      out.write("</td>\r\n");
      out.write("              <td class=\"left\">");
      out.print(a.getHinhAnh());
      out.write("</td>\r\n");
      out.write("              <td class=\"right\">");
      out.print(a.getLoaitt().getTenLoaiTt());
      out.write("</td>\r\n");
      out.write("              <td class=\"right\"> [ <a href=\"#\" class=\"current\">Sửa</a> ]\r\n");
      out.write("                                 [ <a href=\"#\" class=\"current\">Xóa</a> ]\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("                      \r\n");
      out.write("           </tbody>\r\n");
      out.write("           ");

                   }
           
      out.write("\r\n");
      out.write("        </table>\r\n");
      out.write("      </form>\r\n");
      out.write("    </div>\r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("        <!-- ket thuc noidung quan li tin tuc-->\r\n");
      out.write("  \r\n");
      out.write("  </div>\r\n");
      out.write("  <div class=\"col-md-3 col-md-pull-9\" style=\"background: #6a6b72;color: #ffffff;border-right: 5px solid #269abc;height: 900px;\">\r\n");
      out.write("      <!--menu-->\r\n");
      out.write("      <div id=\"tit\">\r\n");
      out.write("          <h3>SweetCoffee</h3>\r\n");
      out.write("          Bảng quản trị<br>\r\n");
      out.write("          <h6>Xin chào,admin</h6>\r\n");
      out.write("          <h6>Xem trang web | Thoát</h6>\r\n");
      out.write("      </div>\r\n");
      out.write("      <div id='cssmenu'>\r\n");
      out.write("<ul>\r\n");
      out.write("    <li class='active'><a href=\"admin.jsp\"><span>Bảng quản trị</span></a></li>\r\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí sản phẩm</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Menu sản phẩm</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Sản phẩm chi tiết</span></a></li>    \r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí bán hàng</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Đơn đặt hàng</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Khách hàng</span></a></li>\r\n");
      out.write("          <li><a href='#'><span>Khuyến mãi</span></a></li>\r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí tin tức</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Tin tức cà phê</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Tin hoạt động</span></a></li>\r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("   <li class='has-sub'><a href='#'><span>Quản lí blog, hình ảnh</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Quản lí Blog</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Hình ảnh lưu niệm</span></a></li>\r\n");
      out.write("         <li><a href='#'><span>Hình ảnh slide</span></a></li>\r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("    <li class='has-sub'><a href='#'><span>Báo cáo , thống kê</span></a>\r\n");
      out.write("      <ul>\r\n");
      out.write("         <li><a href='#'><span>Chưa biết</span></a></li>\r\n");
      out.write("      </ul>\r\n");
      out.write("   </li>\r\n");
      out.write("</ul>\r\n");
      out.write("</div>\r\n");
      out.write("      \r\n");
      out.write("  </div>\r\n");
      out.write("</div>\r\n");
      out.write("        \r\n");
      out.write("         <div class=\"footer\">\r\n");
      out.write("            <div class=\"menu\">\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <br>\r\n");
      out.write("                        Contact| Sweet_Coffee @ 2014\r\n");
      out.write("                </div></div>\r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("    </body>\r\n");
      out.write("    <script src=\"script/js_admin.js\" type=\"text/javascript\"></script>\r\n");
      out.write("  \r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
