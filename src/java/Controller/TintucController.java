/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import DAO.LoaitintucDAO;
import DAO.tintucDAO;
import Entities.Loaitt;
import Entities.Tintuc;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ngoc_Anh
 */
public class TintucController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try{
        String sotrang= request.getParameter("trang");
        if( sotrang !=null ){
            if (Integer.parseInt(sotrang)<0)
            {
                sotrang="0";
            }
            //luu so trang o session
            HttpSession session = request.getSession(false); //khoi tao va lay session cua web
            session.setAttribute("sotrang", Integer.parseInt(sotrang));//insert or edit bien trong session 

            response.sendRedirect("danhsachtintuc.jsp");
        }
         }
        catch(Exception e){}
            try{
                        String cv = request.getParameter("congviec");
            if(cv.equals("themtintuc"))
            {
                response.getWriter().print(request.getParameter("congviec"));
                String idtin=request.getParameter("idloaitin");
                String tieude=request.getParameter("tieude");
                String noidung=request.getParameter("noidung");
                String hinhanh=request.getParameter("hinhanh");
                String modau=request.getParameter("modau");
                Tintuc a= new Tintuc();
                Loaitt loai_tt=new Loaitt();
                try {
                    LoaitintucDAO ltt = new LoaitintucDAO();
                    loai_tt=ltt.getById(Integer.parseInt(idtin), true);
                    loai_tt.setMaLoaiTt(Integer.parseInt(idtin));

                } catch (Exception ex) {
                    Logger.getLogger(TintucController.class.getName()).log(Level.SEVERE, null, ex);
                }
                a.setLoaitt(loai_tt);
                a.setModau(modau);
                a.setHinhAnh(hinhanh);
                a.setTieuDe(tieude);
                a.setNoiDung(noidung);
                a.setBinhluans(null);
                a.setKhuyenmais(null);
                try {
                    tintucDAO tt = new tintucDAO();
                    tt.addNew(a);
                    response.sendRedirect("danhsachtintuc.jsp");
                } catch (Exception ex) {
                    Logger.getLogger(TintucController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if(cv.equals("xoa"))
            {
                String idxoa=request.getParameter("idxoa");
                try {
                    new tintucDAO().delete(new tintucDAO().getById(Integer.parseInt(idxoa), true));
                    response.sendRedirect("admin_qltintuc.jsp");
                } catch (Exception ex) {
                    Logger.getLogger(TintucController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if(cv.equals("sua"))
            {
               String idsua= request.getParameter("idsua");
                try {
                    new tintucDAO().update(new tintucDAO().getById(Integer.parseInt(idsua), true));

                } catch (Exception ex) {
                    Logger.getLogger(TintucController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        catch(Exception e){}
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
