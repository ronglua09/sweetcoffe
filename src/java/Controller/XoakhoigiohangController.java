/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Entities.ChiTietGioHang;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ngoc_Anh
 */
public class XoakhoigiohangController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        //Lay thong tin gio hang da dc luu tren session.
        HttpSession session= request.getSession(false); // Lay session da dc luu ra
        ArrayList<ChiTietGioHang> GioHang= new ArrayList<ChiTietGioHang>();// Tao bien gio hang moi
        // lay gia tri cua gio hang
        Object GioHang_cu= session.getAttribute("giohang");
        // Neu gio hang khong rong thi lay thong tin gio hang cu dua vao bien gio hang xu li moi
        if(GioHang_cu != null)
            GioHang= (ArrayList<ChiTietGioHang>) GioHang_cu;
        
        
         // tim masp can xoa
        int mahangcanxoa=Integer.parseInt(request.getParameter("massp"));
        for(int i=0; i< GioHang.size();i++)
        {
            if(mahangcanxoa == GioHang.get(i).masanpham)
                GioHang.remove(i);
        }
        session.setAttribute("giohang", GioHang);
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
