/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import DAO.*;
import Entities.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
/**
 *
 * @author QUOCDAT_HT
 */
@WebServlet(name = "ThemSanpham", urlPatterns = {"/ThemSanpham"})
public class ThemSanpham extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
         response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet chitiettintucontrolller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet chitiettintucontrolller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
         
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ThemSanpham.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
      //  try {
              
            boolean isMultipart=ServletFileUpload.isMultipartContent(request);
            if(!isMultipart){
            }else{
            FileItemFactory factory=new DiskFileItemFactory();
            ServletFileUpload upload=new ServletFileUpload(factory);
            List items=null;
                try {
                    items=upload.parseRequest(request);
                } catch (FileUploadException e) {
                    e.printStackTrace();
                }
                Iterator iter= items.iterator();
                Hashtable params=new Hashtable();
                String fileName="";
               
                while(iter.hasNext()){
                    FileItem item= (FileItem) iter.next();
                    if(item.isFormField()){
                        params.put(item.getFieldName(),new String(item.getString().getBytes("ISO-8859-1"),"UTF-8"));
                    }else{
                        try {
                            String itemName=item.getName();
                            fileName=itemName.substring(itemName.lastIndexOf("\\")+1);
                            System.out.printf("path"+fileName);
                            String ReadPath=getServletContext().getRealPath("/")+"\\image\\"+fileName;
                            System.out.printf("Rpath"+ReadPath);
                            File savedFile=new File(ReadPath);
                            item.write(savedFile);
                        } catch (Exception e) {
                        }
                         
                         
                
                    }
                    }
             String maloaisp=(String)params.get("txtloaisp");
              String tensp=(String)params.get("txttensanpham");
               String mieuta=(String)params.get("txtmieuta");
              String gia=(String)params.get("txtgia");
              String hinhanh=null;
              hinhanh="image\\"+fileName;
             
              DangkiDAO sp=new DangkiDAO();
              sp.savesp(Integer.parseInt(maloaisp), tensp, mieuta, hinhanh, Integer.parseInt(gia));
              response.getWriter().print("<script> alert('Ban da them san pham thanh cong !!');var my_timeout=setTimeout(\"gotosite();\",10);\n" +"function gotosite()\n" +
"{\n" +
"window.location=\"Sanpham.jsp\";\n" +
"} </script>");
               
            }
           
            
        /*} catch (Exception e) {
        
        }
        */
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
