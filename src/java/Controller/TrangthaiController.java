/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import DAO.*;
import Entities.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author QUOCDAT_HT
 */
@WebServlet(name = "TrangthaiController", urlPatterns = {"/TrangthaiController"})
public class TrangthaiController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TrangthaiController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TrangthaiController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       String ten=request.getParameter("txttrangthai");
        Trangthaidonhang t = new Trangthaidonhang(ten);
     // try {
             TrangThaiDAO trangthai =new TrangThaiDAO();
             trangthai.save(t);
      response.getWriter().print("<script> alert('Ban da dang ky thanh cong !!');var my_timeout=setTimeout(\"gotosite();\",10);\n" +
"function gotosite()\n" +
"{\n" +
"window.location=\"trangchu.jsp\";\n" +
"} </script>");
             
              
            //Khachhang kh=new KhachhangDAO().getKHBysdt(sdt);
            //if(kh.getMaKh()!=0){
            //int makh=kh.getMaKh();
           // Nguoidung d=new Nguoidung(user,makh,pass,1);
           /* try{
                new NguoidungDAO().addNew(d);
            }catch (Exception ex) {
            response.getWriter().print("<script> alert('Loi loi !!');var my_timeout=setTimeout(\"gotosite();\",10);\n" +
"function gotosite()\n" +
"{\n" +
"window.location=\"trangchu.jsp\";\n" +
"} </script>");
            //Logger.getLogger(Taotaikhoan.class.getName()).log(Level.SEVERE, null, ex);
            }
            }*/
         /*   response.getWriter().print("<script> alert('Ban da dang ky thanh cong !!');var my_timeout=setTimeout(\"gotosite();\",10);\n" +
"function gotosite()\n" +
"{\n" +
"window.location=\"trangchu.jsp\";\n" +
"} </script>");
        } catch (Exception ex) {
            response.getWriter().print("<script> alert('Loi loi !!');var my_timeout=setTimeout(\"gotosite();\",10);\n" +
"function gotosite()\n" +
"{\n" +
"window.location=\"dangkitaikhoan.jsp\";\n" +
"} </script>");
            //Logger.getLogger(Taotaikhoan.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
   
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
