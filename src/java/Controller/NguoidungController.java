/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import DAO.NguoidungDAO;
import Entities.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author QUOCDAT_HT
 */
@WebServlet(name = "NguoidungController", urlPatterns = {"/NguoidungController"})
public class NguoidungController extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");   
        String ten = request.getParameter("ten");
        String pass = request.getParameter("pass");
        try {
            Nguoidung n = new NguoidungDAO().getNguoidungByuser(ten);
            if(n.getPass().equals(pass))
            {
                HttpSession session = request.getSession(false); 
                session.setAttribute("user", n);
            }
            else
            {
                response.getWriter().print("<script> alert('Sai ten dang nhap hoac mat khau !!! '); </script>");
            }
            
        } catch (Exception ex) {
            Logger.getLogger(Nguoidung.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.getWriter().print("<script> var my_timeout=setTimeout(\"gotosite();\",10);\n" +
"function gotosite()\n" +
"{\n" +
"window.location=\"admin.jsp\";\n" +
"} </script>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
