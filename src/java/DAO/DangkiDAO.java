/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import DATA.HibernateUtil;
import Entities.*;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author QUOCDAT_HT
 */
public class DangkiDAO {
      SessionFactory factory =HibernateUtil.getSessionFactor();
        Session session =factory.getCurrentSession();
        
        public DangkiDAO(){
        //session=HibernateUtil.getSessionFactor().openSession();
        }
    public void savekh(int lkh,String hoten,String diachi,String sdt,String gt,String email)
    {
        try {
        session.beginTransaction();
        Loaikh loaikh=(Loaikh)session.get(Loaikh.class,lkh);
        Khachhang kh=new Khachhang(loaikh, hoten, diachi, sdt, gt, email);
        loaikh.getKhachhangs().add(kh);
        session.save(kh);
        session.flush();
        session.getTransaction().commit();
        } catch (Exception e) {
            if(session.getTransaction().isActive())
            {
                session.getTransaction().rollback();                
            }
            e.printStackTrace();
        }
    }
    public void savend(String user,int id,String pass,int quyen)
    {
        try {
            
        session.beginTransaction();
        Khachhang kh=(Khachhang)session.get(Khachhang.class,id);
        Phanquyen pk=(Phanquyen)session.get(Phanquyen.class, quyen);
       Nguoidung nd =new Nguoidung(user,kh, pass, pk);
       kh.getNguoidungs().add(nd);
       pk.getNguoidungs().add(nd);
        session.save(nd);
        session.flush();
        session.getTransaction().commit();
        } catch (Exception e) {
            if(session.getTransaction().isActive())
            {
                session.getTransaction().rollback();                
            }
            e.printStackTrace();
        }
    }
    public void savesp(int lsp,String tensp,String mieuta,String hinhanh,float gia)
    {
        try {
        session.beginTransaction();
        Loaisp loaisp=(Loaisp)session.get(Loaisp.class,lsp);
        Sanpham sp=new Sanpham(loaisp, tensp, mieuta, hinhanh, gia);
        loaisp.getSanphams().add(sp);
        session.save(sp);
        session.flush();
        session.getTransaction().commit();
        } catch (Exception e) {
            if(session.getTransaction().isActive())
            {
                session.getTransaction().rollback();                
            }
            e.printStackTrace();
        }
    }
  
}
