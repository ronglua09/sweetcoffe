/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import DATA.ManagerBase;
import Entities.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author QUOCDAT_HT
 */
public class LoaitintucDAO extends ManagerBase<Loaitt>{
    public LoaitintucDAO() throws Exception{
    }
    
    public List getAllLoaitintuc()
    {
        try{
            String[] sort = new String[] {"maLoaiTt desc"};
            List<Loaitt> list = getBySQLQuery(sort, 0);
           
            return list;
        }
        catch(Exception ex){
            Logger.getLogger(Tintuc.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    }
    
    public Loaitt getLoaiTintucByid(Integer id){
        try{
            String query = " MaLoaiTT = '" + id+"'";
            String[] sort = new String[] {};
            List<Loaitt> list = getBySQLQuery(query, sort, 0);
           
            return list.get(0);
        }
        catch(Exception ex){
            Logger.getLogger(Loaitt.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    } 
    
    
}
