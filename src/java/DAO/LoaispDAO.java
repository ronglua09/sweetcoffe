/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import DATA.ManagerBase;
import Entities.Loaisp;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author QUOCDAT_HT
 */
public class LoaispDAO extends ManagerBase<Loaisp>{
    public LoaispDAO() throws Exception{
    }
    
    public List getAllLoaiSP()
    {
        try{
            String[] sort = new String[] {"MaLoaiSP"};
            List<Loaisp> list = getBySQLQuery(sort, 0);
           
            return list;
        }
        catch(Exception ex){
            Logger.getLogger(Loaisp.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    }
    
    public Loaisp getLoaiSPByid(Integer id){
        try{
            String query = " MaLoaiSP = '" + id+"'";
            String[] sort = new String[] {};
            List<Loaisp> list = getBySQLQuery(query, sort, 0);
           
            return list.get(0);
        }
        catch(Exception ex){
            Logger.getLogger(Loaisp.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    } 
    
}
