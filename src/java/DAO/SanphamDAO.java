/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import DATA.HibernateUtil;
import DATA.ManagerBase;
import Entities.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author QUOCDAT_HT
 */
public class SanphamDAO extends ManagerBase<Sanpham>{
    public SanphamDAO() throws Exception{
    }
     public List<Sanpham> getAllSanPham()
    {
         try{
            
            List<Sanpham> list = getAll();
           
            return list;
        }
        catch(Exception ex){
            Logger.getLogger(Sanpham.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    }
    public List<Sanpham> getAllSP()
    {
        try{
            String[] sort = new String[] {"MaSP"};
            List<Sanpham> list = getBySQLQuery(sort, 0);
           
            return list;
        }
        catch(Exception ex){
            Logger.getLogger(Sanpham.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    }
    
    public Sanpham getSPByid(Integer id){
        try{
            String query = " MaSP = '" + id+"'";
            String[] sort = new String[] {};
            List<Sanpham> list = getBySQLQuery(query, sort, 0);
           
            return list.get(0);
        }
        catch(Exception ex){
            Logger.getLogger(Sanpham.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    }
         public List<Sanpham> getSPByMaloaiSP(Integer id){
          try{
            String query = " MaLoaiSP = '" + id+"'";
            String[] sort = new String[] {};
            List<Sanpham> list = getBySQLQuery(query, sort, 0);
           
            return list;
        }
        catch(Exception ex){
            Logger.getLogger(Sanpham.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
         
    } 
      
        
       
    
}
