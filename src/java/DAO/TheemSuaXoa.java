/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import DATA.HibernateUtil;
import Entities.Loaisp;
import Entities.Sanpham;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author QUOCDAT_HT
 */
public class TheemSuaXoa {
     SessionFactory factory =HibernateUtil.getSessionFactor();
        Session session =factory.getCurrentSession();
         public void savesp(int lsp,String tensp,String mieuta,String hinhanh,float gia)
    {
        try {
        session.beginTransaction();
        Loaisp loaisp=(Loaisp)session.get(Loaisp.class,lsp);
        Sanpham sp=new Sanpham(loaisp, tensp, mieuta, hinhanh, gia);
        loaisp.getSanphams().add(sp);
        session.save(sp);
        session.flush();
        session.getTransaction().commit();
        } catch (Exception e) {
            if(session.getTransaction().isActive())
            {
                session.getTransaction().rollback();                
            }
            e.printStackTrace();
        }
    }
}
