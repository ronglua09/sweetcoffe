/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import DATA.HibernateUtil;
import DATA.ManagerBase;
import Entities.Loaisp;
import Entities.Tintuc;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author QUOCDAT_HT
 */
public class tintucDAO extends ManagerBase<Tintuc>{
    SessionFactory factory =HibernateUtil.getSessionFactor();
        Session session =factory.getCurrentSession();
    public tintucDAO() throws Exception{
    }
   public void save(Tintuc t)
    {
        try {
        session.beginTransaction();
        
        
        session.save(t);
        session.flush();
        session.getTransaction().commit();
        } catch (Exception e) {
            if(session.getTransaction().isActive())
            {
                session.getTransaction().rollback();                
            }
            e.printStackTrace();
        }
    }
    public List<Tintuc> getAlltintuc()
    {
        try{
            
            List<Tintuc> list = getAll();
           
            return list;
        }
        catch(Exception ex){
            Logger.getLogger(Tintuc.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    }
    public List<Tintuc> Gettintuctheotrang(int trang)
    {
         try{
             
            int limit= (trang-1)*5; 
            String query = "";
            String[] sort = new String[] {"maTt desc"};
            
            List<Tintuc> list = getBySQLQuery(query, sort, limit, 5);
            return list;
        }
        catch(Exception ex){
            Logger.getLogger(Tintuc.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    }
    public Tintuc getTintucByid(Integer id){
        try{
            String query = " MaTT = '" +id+"'";
            String[] sort = new String[] {};
            List<Tintuc> list = getBySQLQuery(query, sort, 0);
           
            return list.get(0);
        }
        catch(Exception ex){
            Logger.getLogger(Tintuc.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    } 
     public Tintuc getTintucByMaloaiTT(Integer id){
        try{
            String query = " MaLoaiTT = '" + id+"'";
            String[] sort = new String[] {};
            List<Tintuc> list = getBySQLQuery(query, sort, 0);
           
            return list.get(0);
        }
        catch(Exception ex){
            Logger.getLogger(Tintuc.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    } 
}
