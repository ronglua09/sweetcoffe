/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import DATA.HibernateUtil;
import DATA.ManagerBase;
import Entities.Nguoidung;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author QUOCDAT_HT
 */
public class NguoidungDAO extends ManagerBase<Nguoidung>{
    public NguoidungDAO() throws Exception{}
    SessionFactory factory =HibernateUtil.getSessionFactor();
        Session session =factory.getCurrentSession();
     private final HibernateUtil hib = new HibernateUtil();
     private final SessionFactory sf = hib.getSessionFactory();
    public List<Nguoidung> Xemdsnd()
    {
        try
        {
             sf.getCurrentSession().beginTransaction();
            List<Nguoidung> result = sf.getCurrentSession().createCriteria(Nguoidung.class).list();
            return result;
            
        }
        catch(Exception e)
        {
            return null;
        }
        
    }
    public Nguoidung Compare(String nuser,String npass)
    {
         try
        {
            sf.getCurrentSession().beginTransaction();
             String sql="from Nguoidung where Username =? and Pass =?";
             Query query=sf.getCurrentSession().createQuery(sql);
             query.setString(0,nuser);
             query.setString(1,npass);
             Nguoidung result=(Nguoidung)query.uniqueResult();
            sf.getCurrentSession().flush();
            sf.getCurrentSession().getTransaction().commit();
            
            return result;
        }
        catch(Exception e)
        {
            if(sf.getCurrentSession().getTransaction().isActive())
            {
                sf.getCurrentSession().getTransaction().rollback();
                e.printStackTrace();
            }
             return null;
        }}
     public Nguoidung kiemtrauser(String nuser)
    {
         try
        {
            sf.getCurrentSession().beginTransaction();
             String sql="from Nguoidung where Username =?";
             Query query=sf.getCurrentSession().createQuery(sql);
             query.setString(0,nuser);
             Nguoidung result=(Nguoidung)query.uniqueResult();
            sf.getCurrentSession().flush();
            sf.getCurrentSession().getTransaction().commit();
            
            return result;
        }
        catch(Exception e)
        {
            if(sf.getCurrentSession().getTransaction().isActive())
            {
                sf.getCurrentSession().getTransaction().rollback();
                e.printStackTrace();
            }
             return null;
        }}
     public Nguoidung getNguoidungByuser(String user){
        try{
            String query = " Username = '" + user+"'" +"and MaQuyen=1" ;
            String[] sort = new String[] {};
            List<Nguoidung> list = getBySQLQuery(query, sort, 0);
            return list.get(0);
        }
        catch(Exception ex){
            Logger.getLogger(Nguoidung.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    }
     public Nguoidung gettaikhoannguoidung(String user){
        try{
            String query = " Username = '" + user+"'" +"and MaQuyen=2" ;
            String[] sort = new String[] {};
            List<Nguoidung> list = getBySQLQuery(query, sort, 0);
            return list.get(0);
        }
        catch(Exception ex){
            Logger.getLogger(Nguoidung.class.getName()).log(Level.SEVERE,null,ex);
            return null;
        }
    }
    
}
