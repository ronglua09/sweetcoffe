/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DATA;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Raptor
 */
public class HibernateUtil {
 private static final SessionFactory sessionFactory1;
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            sessionFactory1 = new AnnotationConfiguration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactor() {
        return sessionFactory1;
    }
    private SessionFactory sessionFactory;
    private Session session;
    private String sessionFactoryConfigPath;
    
    protected LockMode lockMode;
    protected Order order;
    protected Criteria criteria;
    protected Restrictions restritions;
    
   public HibernateUtil() throws Exception
   {
       sessionFactoryConfigPath = "";
       sessionFactory = new Configuration().configure().buildSessionFactory();
   }
   public HibernateUtil(String sessionFactoryConfigPath)
   {
       this.sessionFactoryConfigPath = sessionFactoryConfigPath;
       sessionFactory = new Configuration().configure(sessionFactoryConfigPath).buildSessionFactory();
   }
   
   protected void beginTransaction()
   {
       session = sessionFactory.getCurrentSession();
       session.beginTransaction();
   }
   protected void commitAndClose()
   {
       if(session != null)
       {
           session.getTransaction().commit();
           if(session.isOpen())
           {
               session.close();
           }
       }
   }
   protected Session getCurrentSession() throws Exception
   {
       if(session == null){
           if(sessionFactory == null){
               if(sessionFactoryConfigPath == null || sessionFactoryConfigPath.equals("")){
                   sessionFactory = new Configuration().configure().buildSessionFactory();
               }
               else{
                   sessionFactory = new Configuration().configure(this.sessionFactoryConfigPath).buildSessionFactory();
               }
           }session = sessionFactory.getCurrentSession();
       }
       return session;
   }
    public SessionFactory getSessionFactory() throws Exception {
        if(sessionFactory == null){
               if(sessionFactoryConfigPath == null || sessionFactoryConfigPath.equals("")){
                   sessionFactory = new Configuration().configure().buildSessionFactory();
               }
               else{
                   sessionFactory = new Configuration().configure(this.sessionFactoryConfigPath).buildSessionFactory();
               }
           }
       return sessionFactory;
    }
}
