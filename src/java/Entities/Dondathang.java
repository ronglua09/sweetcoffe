package Entities;
// Generated Jun 4, 2014 11:05:27 PM by Hibernate Tools 3.6.0


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Dondathang generated by hbm2java
 */
public class Dondathang  implements java.io.Serializable {


     private Integer maDh;
     private Khachhang khachhang;
     private Trangthaidonhang trangthaidonhang;
     private Date ngayHd;
     private String noiGiao;
     private float tongTien;
     private Set<Giohang> giohangs = new HashSet<Giohang>(0);
     private Set<Chitietdh> chitietdhs = new HashSet<Chitietdh>(0);

    public Dondathang() {
    }

	
    public Dondathang(Khachhang khachhang, Trangthaidonhang trangthaidonhang, Date ngayHd, String noiGiao, float tongTien) {
        this.khachhang = khachhang;
        this.trangthaidonhang = trangthaidonhang;
        this.ngayHd = ngayHd;
        this.noiGiao = noiGiao;
        this.tongTien = tongTien;
    }
    public Dondathang(Khachhang khachhang, Trangthaidonhang trangthaidonhang, Date ngayHd, String noiGiao, float tongTien, Set<Giohang> giohangs, Set<Chitietdh> chitietdhs) {
       this.khachhang = khachhang;
       this.trangthaidonhang = trangthaidonhang;
       this.ngayHd = ngayHd;
       this.noiGiao = noiGiao;
       this.tongTien = tongTien;
       this.giohangs = giohangs;
       this.chitietdhs = chitietdhs;
    }
   
    public Integer getMaDh() {
        return this.maDh;
    }
    
    public void setMaDh(Integer maDh) {
        this.maDh = maDh;
    }
    public Khachhang getKhachhang() {
        return this.khachhang;
    }
    
    public void setKhachhang(Khachhang khachhang) {
        this.khachhang = khachhang;
    }
    public Trangthaidonhang getTrangthaidonhang() {
        return this.trangthaidonhang;
    }
    
    public void setTrangthaidonhang(Trangthaidonhang trangthaidonhang) {
        this.trangthaidonhang = trangthaidonhang;
    }
    public Date getNgayHd() {
        return this.ngayHd;
    }
    
    public void setNgayHd(Date ngayHd) {
        this.ngayHd = ngayHd;
    }
    public String getNoiGiao() {
        return this.noiGiao;
    }
    
    public void setNoiGiao(String noiGiao) {
        this.noiGiao = noiGiao;
    }
    public float getTongTien() {
        return this.tongTien;
    }
    
    public void setTongTien(float tongTien) {
        this.tongTien = tongTien;
    }
    public Set<Giohang> getGiohangs() {
        return this.giohangs;
    }
    
    public void setGiohangs(Set<Giohang> giohangs) {
        this.giohangs = giohangs;
    }
    public Set<Chitietdh> getChitietdhs() {
        return this.chitietdhs;
    }
    
    public void setChitietdhs(Set<Chitietdh> chitietdhs) {
        this.chitietdhs = chitietdhs;
    }




}


