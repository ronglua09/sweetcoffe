package Entities;
// Generated Jun 4, 2014 11:05:27 PM by Hibernate Tools 3.6.0


import java.util.HashSet;
import java.util.Set;

/**
 * Trangthaidonhang generated by hbm2java
 */
public class Trangthaidonhang  implements java.io.Serializable {


     private Integer maTrangThai;
     private String tenTrangThai;
     //private Set<Dondathang> dondathangs = new HashSet<Dondathang>(0);

    public Trangthaidonhang() {
    }

	
    public Trangthaidonhang(String tenTrangThai) {
        this.tenTrangThai = tenTrangThai;
    }
   /* public Trangthaidonhang(String tenTrangThai, Set<Dondathang> dondathangs) {
       this.tenTrangThai = tenTrangThai;
       //this.dondathangs = dondathangs;
    }*/
   
    public Integer getMaTrangThai() {
        return this.maTrangThai;
    }
    
    public void setMaTrangThai(Integer maTrangThai) {
        this.maTrangThai = maTrangThai;
    }
    public String getTenTrangThai() {
        return this.tenTrangThai;
    }
    
    public void setTenTrangThai(String tenTrangThai) {
        this.tenTrangThai = tenTrangThai;
    }
    /*public Set<Dondathang> getDondathangs() {
        return this.dondathangs;
    }
    
    public void setDondathangs(Set<Dondathang> dondathangs) {
        this.dondathangs = dondathangs;
    }
*/



}


