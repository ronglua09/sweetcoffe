package Entities;
// Generated Jun 4, 2014 11:05:27 PM by Hibernate Tools 3.6.0


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Khuyenmai generated by hbm2java
 */
public class Khuyenmai  implements java.io.Serializable {


     private Integer maKm;
     private Tintuc tintuc;
     private Sanpham sanpham;
     private float tiLeKhauHao;
     private Date ngayBd;
     private Date ngayKt;
     private Set<Chitietdh> chitietdhs = new HashSet<Chitietdh>(0);

    public Khuyenmai() {
    }

	
    public Khuyenmai(Tintuc tintuc, Sanpham sanpham, float tiLeKhauHao, Date ngayBd, Date ngayKt) {
        this.tintuc = tintuc;
        this.sanpham = sanpham;
        this.tiLeKhauHao = tiLeKhauHao;
        this.ngayBd = ngayBd;
        this.ngayKt = ngayKt;
    }
    public Khuyenmai(Tintuc tintuc, Sanpham sanpham, float tiLeKhauHao, Date ngayBd, Date ngayKt, Set<Chitietdh> chitietdhs) {
       this.tintuc = tintuc;
       this.sanpham = sanpham;
       this.tiLeKhauHao = tiLeKhauHao;
       this.ngayBd = ngayBd;
       this.ngayKt = ngayKt;
       this.chitietdhs = chitietdhs;
    }
   
    public Integer getMaKm() {
        return this.maKm;
    }
    
    public void setMaKm(Integer maKm) {
        this.maKm = maKm;
    }
    public Tintuc getTintuc() {
        return this.tintuc;
    }
    
    public void setTintuc(Tintuc tintuc) {
        this.tintuc = tintuc;
    }
    public Sanpham getSanpham() {
        return this.sanpham;
    }
    
    public void setSanpham(Sanpham sanpham) {
        this.sanpham = sanpham;
    }
    public float getTiLeKhauHao() {
        return this.tiLeKhauHao;
    }
    
    public void setTiLeKhauHao(float tiLeKhauHao) {
        this.tiLeKhauHao = tiLeKhauHao;
    }
    public Date getNgayBd() {
        return this.ngayBd;
    }
    
    public void setNgayBd(Date ngayBd) {
        this.ngayBd = ngayBd;
    }
    public Date getNgayKt() {
        return this.ngayKt;
    }
    
    public void setNgayKt(Date ngayKt) {
        this.ngayKt = ngayKt;
    }
    public Set<Chitietdh> getChitietdhs() {
        return this.chitietdhs;
    }
    
    public void setChitietdhs(Set<Chitietdh> chitietdhs) {
        this.chitietdhs = chitietdhs;
    }




}


