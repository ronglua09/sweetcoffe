<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Menu chi tiet</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <!-- bootstrap -->
        <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
   <link href="style/style.css" rel="stylesheet" />
   <link href="style/menu_style.css" rel="stylesheet" />
   <script src="script/jquery.js" type="text/javascript"></script>
   <script src="script/script.js" type="text/javascript"></script>
   
    </head>
    <body>
        <div class="container">
            <div class="header">
                 <%@include file="header.jsp" %>
            </div>
            <!--header-->
            <br>
            <div class="menu">
               <%@include file="menungang.jsp" %>
 
            </div>
                
            <!--menu-->
            <br>
            <div class="content">
                <div id="title">
                                 >> Đăng ký tài khoản
                             </div>
                <br>
               <form name="taikhoan"  method="POST"  class="registrations" action="DangkiController" accept-charset="UTF-8">
                <div class="contact">
                    <div id="nhapdulieu">
                        <div id="kh_left">
                            <br>
                            Họ Tên: 
                            <br><br><br>
                            Địa chỉ:
                            <br><br><br>
                             Giới tính:
                            <br><br><br>
                            Email :
                            <br><br>
                            Số Điên Thoại:
                            <br><br>
                           
                        </div>
                        <div id="kh_right">
                            <input type="hidden" class="form-control" id="id" name="id">
                             <br>
                             <input type="text" class="form-control" id="hoten" name="hoten" placeholder="Nhập họ tên" style="width: 300px;"> 
                            <br>
                            <select class="form-control" name="diachi" style="width: 300px;" >
                                <option>Hồ Chí Minh</option>
                                <option>Khác</option>
                            </select>  <br>
                              <select class="form-control" name="gioitinh" style="width: 300px;" >
                                <option>Nam</option>
                                <option>Nữ</option>
                            </select> <br>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Nhập Email" style="width: 300px;">
                           <br>
                           <input type="text" class="form-control" id="sdt" name="sdt" placeholder="Thông tin bắt buộc*" style="width: 300px;">
                          
                        </div>
                    </div>
                    <div id="chuthich">
                       
*Nhập thông tin tài khoản
                            </div>
                     
                  
                    <br>
                    <input type="submit" value="Đồng Ý" style="margin-left: 20px;"/>
                </div>
                </form>
                
            </div>
                
            <!--content-->
            <br>
            <div class="footer">
                <div class="container">
                    <h3><label class="label label-success">SWEET COFFEE SHOP</label></h3>
                </div>
            </div>
            <!--footer-->
        </div>
        <script type="text/javascript" src="script/jquery-min.js"></script>
    </body>
</html>


