<%-- 
    Document   : Xemchitiethoadon
    Created on : Jun 28, 2014, 4:21:48 PM
    Author     : Ngoc_Anh
--%>

<%@page import="java.util.*"%>
<%@page import="Entities.*"%>
<%@page import="DAO.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin_Baocao</title>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
         <link href="style/style_admin.css" rel="stylesheet" />
         <link href="style/style_ad.css" rel="stylesheet" />
          <link href="style/stylesheet.css" rel="stylesheet" />
   <script src="script/jquery.js" type="text/javascript"></script>
   <script language="javascript">
    function chitiet(i)
{
document.forms[i].submit();
}
</script>
    </head
    
    <body>
          <%
       
       Nguoidung acc = null;
       try{
           
       if("logout".equals(request.getParameter("action")))
       {
           session.removeAttribute("user");
       }    
       
       acc = (Nguoidung) session.getAttribute("user");
       }catch(Exception e){}
       
       if(acc == null)
       {
           %>
           <form action="NguoidungController" method="post" id="dangnhap">
               <aside style="height: 700px;background:url(img/bg-login.gif);color: #ffffff;">
               <div class="user" style="height: 200px;background: #101010;">
                   <div id="top" style="margin-left: 500px;">
                       <img alt="Admin" src="img/logotrong.png"/><br><br>
    
      ĐĂNG NHẬP VÀO HỆ THỐNG
    </p>
                   </div>
               </div><br>
    
                   <div id="main-nav" style="margin-left: 500px">
                       <div id="left" style="float: left">
                           <a>Tên đăng nhập :</a> <br><br>
                           <a> Mật khẩu : </a> <br><br>
                       </div>
                       <div id="right">
                              <input type="text" id="ten" name="ten" /><br><br>
                                <input type="password" id="pass" name="pass" /><br><br>
                       </div>
                       <a>
                                <input type="submit" id="dangnhap" name="dangnhap" style="width: 100px;margin-left: 50px;" /></a></li>
       
    </div>
         
</aside>
           
           </form>
           <%
       }
       else
       {
    %>
      
      <div class="row">
  <div class="col-md-9 col-md-push-3">
      <br><br>
        <div id="content" style="width: 1000px;">
  <div class="breadcrumb">
        <a href="#">Trang chủ</a>
         :: <a href="#" class="current">Báo cáo </a>
      </div>
      <div class="box">
    <div class="heading">
      <h1><img src="img/news.png" alt="">Báo cáo khách hàng đặt mua</h1>
      
    </div>
    <div class="content">
      <form action="#" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
                <td class="center">Tên Sản Phẩm</td>
              <td class="center">Mã Khuyến Mãi</td>    
              <td style="text-align: center;width: 300px">Số Lượng</td>
              
            </tr>
          </thead>
          <tbody>
                 
              <%
                  int mahd = Integer.parseInt(request.getParameter("mahd"));
                  
                  List<Chitietdh> _chitietdathang= new  ChitietdhDAO().getAll();
                   for(Chitietdh a:_chitietdathang)
                   {
                       if(a.getDondathang().getMaDh() == mahd)
                       {
              %>
            <tr>
              <td class="center"><%=a.getSanpham().getTenSp()%></td>
              <td class="center"><%if(a.getKhuyenmai() != null)  
                                    %><%=a.getKhuyenmai().getMaKm()%>
                  
              </td>
              <td class="left"><%=a.getSoLuong()%></td>
              
              
            </tr>
                      
           </tbody>
           <%
                       }
                    }
           %>
        </table>
      </form>
    </div>
  </div>
</div>
    </div>
        <div class="col-md-3 col-md-pull-9" style="background: #6a6b72;color: #ffffff;border-right: 5px solid #269abc;height: 900px;">
      <!--menu-->
      <div id="tit">
          <h3>SweetCoffee</h3>
          Bảng quản trị<br>
          <h6>Xin chào,admin</h6>
          <h6>Xem trang web | Thoát</h6>
      </div>
      <div id='cssmenu'>
<ul>
    <li class='active'><a href="Sanpham.jsp"><span>Bảng quản trị</span></a></li>
   <li class='has-sub'><a href='#'><span>Quản lí sản phẩm</span></a>
      <ul>
         <li><a href='#'><span>Menu sản phẩm</span></a></li>
         <li><a href='#'><span>Sản phẩm chi tiết</span></a></li>    
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí bán hàng</span></a>
      <ul>
         <li><a href='#'><span>Đơn đặt hàng</span></a></li>
         <li><a href='#'><span>Khách hàng</span></a></li>
          <li><a href='#'><span>Khuyến mãi</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí tin tức</span></a>
      <ul>
         <li><a href='#'><span>Tin tức cà phê</span></a></li>
         <li><a href='#'><span>Tin hoạt động</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí blog, hình ảnh</span></a>
      <ul>
         <li><a href='#'><span>Quản lí Blog</span></a></li>
         <li><a href='#'><span>Hình ảnh lưu niệm</span></a></li>
         <li><a href='#'><span>Hình ảnh slide</span></a></li>
      </ul>
   </li>
    <li class='has-sub'><a href='#'><span>Báo cáo , thống kê</span></a>
      <ul>
         <li><a href='#'><span>Chưa biết</span></a></li>
      </ul>
   </li>
</ul>
</div>
      
  </div>
</div>
        <!--nội dung-->
        
        <!-- ket thuc noidung -->
         <div class="footer">
            <div class="menu">
                <div class="container">
                    <br>
                        Contact| Sweet_Coffee @ 2014
                </div></div>
        </div>
        <%}%>
    </body>
    <script src="script/js_admin.js" type="text/javascript"></script>
        
</html>

