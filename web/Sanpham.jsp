<%-- 
    Document   : admin
    Created on : Jun 4, 2014, 11:16:07 PM
    Author     : QUOCDAT_HT
--%>

<%@page import="java.util.*"%>
<%@page import="Entities.*"%>
<%@page import="DAO.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>admin</title>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
         <link href="style/style_admin.css" rel="stylesheet" />
         <link href="style/style_ad.css" rel="stylesheet" />
          <link href="style/stylesheet.css" rel="stylesheet" />
   <script src="script/jquery.js" type="text/javascript"></script>
   <script language="javascript">
    function chitiet(i)
{
document.forms[i].submit();
}
</script>
    </head
    
    <body>
          <%
       
       Nguoidung acc = null;
       try{
           
       if("logout".equals(request.getParameter("action")))
       {
           session.removeAttribute("user");
       }    
       
       acc = (Nguoidung) session.getAttribute("user");
       }catch(Exception e){}
       
       if(acc == null)
       {
           %>
           <form action="NguoidungController" method="post" id="dangnhap">
               <aside style="height: 700px;background:url(img/bg-login.gif);color: #ffffff;">
               <div class="user" style="height: 200px;background: #101010;">
                   <div id="top" style="margin-left: 500px;">
                       <img alt="Admin" src="img/logotrong.png"/><br><br>
    
      ĐĂNG NHẬP VÀO HỆ THỐNG
    </p>
                   </div>
               </div><br>
    
                   <div id="main-nav" style="margin-left: 500px">
                       <div id="left" style="float: left">
                           <a>Tên đăng nhập :</a> <br><br>
                           <a> Mật khẩu : </a> <br><br>
                       </div>
                       <div id="right">
                              <input type="text" id="ten" name="ten" /><br><br>
                                <input type="password" id="pass" name="pass" /><br><br>
                       </div>
                       <a>
                                <input type="submit" id="dangnhap" name="dangnhap" style="width: 100px;margin-left: 50px;" /></a></li>
       
    </div>
         
</aside>
           
           </form>
           <%
       }
       else
       {
    %>
      
      <div class="row">
  <div class="col-md-9 col-md-push-3">
      <br><br>
        <div id="content" style="width: 1000px;">
  <div class="breadcrumb">
        <a href="#">Trang chủ</a>
         :: <a href="#" class="current">Sản Phẩm</a>
      </div>
      <div class="box">
    <div class="heading">
      <h1><img src="img/news.png" alt=""> Sản Phẩm</h1>
      <div class="buttons"><a onclick="location = '#'" class="button">Thêm</a><a onclick="$('#form').attr('action', '#'); $('#form').submit();" class="button">Sao chép</a><a onclick="$('form').submit();" class="button">Xóa</a></div>
    </div>
    <div class="content">
      <form action="#" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
                <td class="center">Mã Sản Phẩm</td>
              <td class="center">Tên Sản Phẩm</td>    
              <td style="text-align: center;width: 300px">Miêu Tả</td>
              <td class="center" style="text-align: center;width: 200px">Hình Ảnh</td>
              <td class="center">Giá</td>
              <td class="center">Loại Sản Phẩm</td>
              <td class="center">Thao Tác</td>
            </tr>
          </thead>
          <tbody>
                 
              <%
                  int Stt = 1;
                  List<Sanpham> _sanpham= new  SanphamDAO().getAllSanPham();
                   for(Sanpham a:_sanpham)
                   {
                       
              %>
            <tr>
              <td class="center"><%=a.getMaSp()%></td>
              <td class="center"><%=a.getTenSp()%></td>
              <td class="center"><%=a.getMieuTa()%></td>
              <td class="left"><%=a.getHinhAnh()%></td>
              <td class="left"><%=a.getGia()%></td>
              <td class="right"><%=a.getLoaisp().getTenLoaiSp()%></td>
              <td class="right"> [
                  <form action="Qlsanpham" method="POST">
                      <input type="hidden" name="getMaSp" value="<%=a.getMaSp()%>"/>
                       <b>
                        <a href="#" class="current" onclick="chitiet(<%=Stt%>);">Sửa</a>
                        </b>
                   </form>]
                                 [ <a href="#" class="current">Xóa</a> ]
                </td>
            </tr>
                      
           </tbody>
           <%
                    Stt++;  }
           %>
        </table>
      </form>
    </div>
  </div>
</div>
    </div>
        <div class="col-md-3 col-md-pull-9" style="background: #6a6b72;color: #ffffff;border-right: 5px solid #269abc;height: 900px;">
      <!--menu-->
      <div id="tit">
          <h3>SweetCoffee</h3>
          Bảng quản trị<br>
          <h6>Xin chào,admin</h6>
          <h6>Xem trang web | Thoát</h6>
      </div>
      <div id='cssmenu'>
<ul>
    <li class='active'><a href="Sanpham.jsp"><span>Bảng quản trị</span></a></li>
   <li class='has-sub'><a href='#'><span>Quản lí sản phẩm</span></a>
      <ul>
         <li><a href='#'><span>Menu sản phẩm</span></a></li>
         <li><a href='#'><span>Sản phẩm chi tiết</span></a></li>    
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí bán hàng</span></a>
      <ul>
         <li><a href='#'><span>Đơn đặt hàng</span></a></li>
         <li><a href='#'><span>Khách hàng</span></a></li>
          <li><a href='#'><span>Khuyến mãi</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí tin tức</span></a>
      <ul>
         <li><a href='#'><span>Tin tức cà phê</span></a></li>
         <li><a href='#'><span>Tin hoạt động</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí blog, hình ảnh</span></a>
      <ul>
         <li><a href='#'><span>Quản lí Blog</span></a></li>
         <li><a href='#'><span>Hình ảnh lưu niệm</span></a></li>
         <li><a href='#'><span>Hình ảnh slide</span></a></li>
      </ul>
   </li>
    <li class='has-sub'><a href='#'><span>Báo cáo , thống kê</span></a>
      <ul>
         <li><a href='#'><span>Chưa biết</span></a></li>
      </ul>
   </li>
</ul>
</div>
      
  </div>
</div>
        <!--nội dung-->
        
        <!-- ket thuc noidung -->
         <div class="footer">
            <div class="menu">
                <div class="container">
                    <br>
                        Contact| Sweet_Coffee @ 2014
                </div></div>
        </div>
        <%}%>
    </body>
    <script src="script/js_admin.js" type="text/javascript"></script>
        
</html>
