<%-- 
    Document   : admin_themtintuc
    Created on : Jun 29, 2014, 11:41:35 AM
    Author     : Ngoc_Anh
--%>



<%@page import="Entities.Loaitt"%>
<%@page import="java.util.List"%>
<%@page import="Entities.Tintuc"%>
<%@page import="DAO.tintucDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>admin</title>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
         <link href="style/style_admin.css" rel="stylesheet" />
         <link href="style/stylesheet.css" rel="stylesheet" />
         <link href="style/style_ad.css" rel="stylesheet" />
         
   <script src="script/jquery.js" type="text/javascript"></script>
  <script src="ckeditor/ckeditor.js" type="text/javascript"></script>
   <script src="ckfinder/ckfinder.js" type="text/javascript"></script>
  
    </head>
    
    <body>
        <div class="row">
  <div class="col-md-9 col-md-push-3">
  
      <!--nội dung quan li tin tuc-->
        <div id="content" style="width: 1000px;">
  <div class="breadcrumb">
        <a href="#">Trang chủ</a>
         :: <a href="#" class="current">Tin tức</a>
      </div>
      <!-- khung them tin tuc-->
      
      <div class="box">
            <div class="heading">
              <h1><img src="img/news.png" alt=""> Tin tức</h1>
              <div class="buttons"><a onclick="$('#form').submit();" class="button">Lưu</a>
                  <a onclick="location = '';" class="button">Hủy</a>
              </div>
            </div>
        <div class="content">
          <div id="tabs" class="htabs"><a href="#tab-general" class="" style="display: inline;">Chi Tiết Thêm</a>
          </div>
          <form action="ThemtintucController" method="POST" enctype="multipart/form-data" id="form">
              <input type="text" name="congviec" id="congviec" value="themtintuc" hidden="true"/>
                <table class="form">
                  <tbody><tr class="alt-row">
                    <td><span class="required">*</span> Loại Tin Tức :</td>
                    <td>
                       <select id="idloaitin" name="idloaitin">
                            <jsp:useBean id="_loaitintuc" class="DAO.LoaitintucDAO" scope="page"/> 
                           <%
                               List<Loaitt> _list= _loaitintuc.getAllLoaitintuc();
                               if(_list!=null)
                                   for(Loaitt a:_list)
                                   {

                            %>  

                            <option value="<%=a.getMaLoaiTt()%>"><%=a.getTenLoaiTt()%></option>
                              <%
                                  }
                              %>     
                            </select>
                      </td>
                  </tr>
                  <tr>
                      <td>Tiêu Đề:</td>
                    <td><textarea id="tieude" name="tieude" cols="40" rows="5"></textarea></td>
                  </tr>
                  <tr class="alt-row">
                    <td>Phần Mở Đầu: </td>
                    <td><textarea id="modau" name="modau" cols="40" rows="5"></textarea></td>
                  </tr>
                  <tr>
                    <td>Nội Dung : </td>
                    <td>
                        
                        <textarea name="noidung" class="ckeditor" /></textarea>
                    </td>
                  </tr>
                  <tr class="alt-row">
                    <td>Hình Ảnh :</td>
                    <td>
      <input type="file" name="datafile" accept="image/gif, image/jpeg, image/jpg, image/png, image/bmp" ></td>
                  </tr>
                </tbody></table>
          </form>
        </div>
  </div>
      <!--ket thuc khung them tin tuc-->
      
</div>
        <!-- ket thuc noidung quan li tin tuc-->
  
  </div>
  <div class="col-md-3 col-md-pull-9" style="background: #6a6b72;color: #ffffff;border-right: 5px solid #269abc;height: 900px;">
      <!--menu-->
      <div id="tit">
          <h3>SweetCoffee</h3>
          Bảng quản trị<br>
          <h6>Xin chào,admin</h6>
          <h6>Xem trang web | Thoát</h6>
      </div>
      <div id='cssmenu'>
<ul>
    <li class='active'><a href="admin.jsp"><span>Bảng quản trị</span></a></li>
   <li class='has-sub'><a href='#'><span>Quản lí sản phẩm</span></a>
      <ul>
         <li><a href='#'><span>Menu sản phẩm</span></a></li>
         <li><a href='#'><span>Sản phẩm chi tiết</span></a></li>    
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí bán hàng</span></a>
      <ul>
         <li><a href='#'><span>Đơn đặt hàng</span></a></li>
         <li><a href='#'><span>Khách hàng</span></a></li>
          <li><a href='#'><span>Khuyến mãi</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí tin tức</span></a>
      <ul>
         <li><a href='#'><span>Tin tức cà phê</span></a></li>
         <li><a href='#'><span>Tin hoạt động</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Quản lí blog, hình ảnh</span></a>
      <ul>
         <li><a href='#'><span>Quản lí Blog</span></a></li>
         <li><a href='#'><span>Hình ảnh lưu niệm</span></a></li>
         <li><a href='#'><span>Hình ảnh slide</span></a></li>
      </ul>
   </li>
    <li class='has-sub'><a href='#'><span>Báo cáo , thống kê</span></a>
      <ul>
         <li><a href='#'><span>Chưa biết</span></a></li>
      </ul>
   </li>
</ul>
</div>
      
  </div>
</div>
        
                           
         <div class="footer">
            <div class="menu">
                <div class="container">
                    <br>
                        Contact| Sweet_Coffee @ 2014
                </div></div>
        </div>
        
    </body>
    <script src="script/js_admin.js" type="text/javascript"></script>
  
    <script type="text/javascript">
        function BrowseServer() {
            var finder = new CKFinder();
            //finder.basePath = '../';
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }
        function SetFileField(fileUrl) {
            document.getElementById('Image').value = fileUrl;
        }
    </script>
    
  
</html>

