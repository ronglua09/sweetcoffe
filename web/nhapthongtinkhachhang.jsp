<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Menu chi tiet</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <!-- bootstrap -->
        <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
   <link href="style/style.css" rel="stylesheet" />
   <link href="style/menu_style.css" rel="stylesheet" />
   <script src="script/jquery.js" type="text/javascript"></script>
   <script src="script/script.js" type="text/javascript"></script>
   
    </head>
    <body>
        <div class="container">
            <div class="header">
                
                 <%@include file="header.jsp" %>
                </div>
            
            <!--header-->
            <br>
            <div class="menu">
               <%@include file="menungang.jsp" %>
            </div>
                
            <!--menu-->
            <br>
            <div class="content">
                <div id="title">
                                 >> Thông tin khách hàng
                             </div>
                <br>
                <div class="contact">
                    <div id="nhapdulieu">
                        <div id="kh_left">
                            <br>
                            Họ Tên: 
                            <br><br><br>
                            Email :
                            <br><br><br>
                            Số Điên Thoại:
                            <br><br><br>
                            Địa chỉ: 
                            <br><br>
                            Tỉnh, thành phố:
                            <br>
                        </div>
                        <div id="kh_right">
                             <br>
                             <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nhập họ tên" style="width: 300px;"> 
                            <br>
                           <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Nhập Email, nếu có" style="width: 300px;">
                            <br>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Thông tin bắt buộc*" style="width: 300px;">
                           <br>
                           <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nhập địa chỉ giao hàng" style="width: 500px;">
                            <br>
                            <select class="form-control" style="width: 300px;" >
                                <option>Hồ Chí Minh</option>
                                <option>Khác</option>
                            </select>
                            <br>
                        </div>
                    </div>
                    <div id="chuthich">
                        
                        <label>
                            <input style="margin-left: 20px;margin-top: 10px;" type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                             Trả tiền khi nhận hàng tận nơi
                        </label>
                    </div>
                    <br>
                    <button style="margin-left: 20px;" type="button" class="btn btn-success">Đồng Ý</button>
                </div>
            </div>
                
            <!--content-->
            <br>
            <div class="footer">
                <div class="container">
                    <h3><label class="label label-success">SWEET COFFEE SHOP</label></h3>
                </div>
            </div>
            <!--footer-->
        </div>
        <script type="text/javascript" src="script/jquery-min.js"></script>
    </body>
</html>


